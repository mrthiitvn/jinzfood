﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace FRAMEWORK.Common.Upload
{
    public class FileUploadHelper
    {
        public static async Task<IEnumerable<string>> FileUpload(HttpRequestMessage request, string filePath)
        {
            var folderDate = DateTime.Now;
            filePath = filePath + "/" + folderDate.ToString("yyyy") + "/" + folderDate.ToString("MM") + "/" + folderDate.ToString("dd") + "/";
            List<string> result = new List<string>();
            var uploadPath = HttpContext.Current.Server.MapPath("~" + filePath);
            if (!Directory.Exists(uploadPath))
            {
                Directory.CreateDirectory(uploadPath);
            }
            var multipartFormDataStreamProvider = new UploadMultipartFormProvider(uploadPath);
            await request.Content.ReadAsMultipartAsync(multipartFormDataStreamProvider);

            var localFileNames = multipartFormDataStreamProvider
                .FileData.Select(multiPartData => multiPartData.LocalFileName).ToList();

            foreach (var localFileName in localFileNames)
            {
                result.Add(filePath + Path.GetFileName(localFileName));
            }
            return result;
        }

        /// <summary>
        /// File Upload By String
        /// </summary>
        /// <param name="imageData"></param>
        /// <param name="imageName"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static string FileUploadByString(string imageData, string imageName, string filePath)
        {
            //var uploadedImagePath = "/Assets/Images/Albums/";
            var folderDate = DateTime.Now;
            filePath = filePath + "/" + folderDate.ToString("yyyy") + "/" + folderDate.ToString("MM") + "/" + folderDate.ToString("dd") + "/";
            var uploadedImagePath = filePath;
            if (!Directory.Exists(uploadedImagePath))
            {
                Directory.CreateDirectory(uploadedImagePath);
            }
            var data = imageData.Substring(imageData.IndexOf(",") + 1);
            var newFile = Convert.FromBase64String(data);
            var fileName = Guid.NewGuid().ToString() + "_" + Path.GetFileName(imageName);
            var dataFile = HttpContext.Current.Server.MapPath(@"~" + uploadedImagePath + fileName);

            System.IO.File.WriteAllBytes(dataFile, newFile);
            string url = uploadedImagePath + Path.GetFileName(dataFile);
            return url;
        }
    }

    public class UploadMultipartFormProvider : MultipartFormDataStreamProvider
    {
        public UploadMultipartFormProvider(string rootPath) : base(rootPath) { }
        public override string GetLocalFileName(HttpContentHeaders headers)
        {
            if (headers != null &&
                headers.ContentDisposition != null)
            {
                return Guid.NewGuid() + "_" + headers
                    .ContentDisposition
                    .FileName.TrimEnd('"').TrimStart('"');
            }
            return base.GetLocalFileName(headers);
        }
    }
    public class UrlHelper
    {
        public static string GetAppDomain()
        {
            //Return variable declaration
            var appPath = string.Empty;

            //Getting the current context of HTTP request
            var context = HttpContext.Current;

            //Checking the current context content
            if (context != null)
            {
                //Formatting the fully qualified website url/name
                appPath = string.Format("{0}://{1}{2}{3}",
                                        context.Request.Url.Scheme,
                                        context.Request.Url.Host,
                                        context.Request.Url.Port == 80
                                            ? string.Empty
                                            : ":" + context.Request.Url.Port,
                                        context.Request.ApplicationPath);
            }

            if (!appPath.EndsWith("/"))
                appPath += "/";

            return appPath;
        }
        public static string GetImageUrl(string localUrl)
        {
            return !string.IsNullOrWhiteSpace(localUrl) ? GetAppDomain() + localUrl : null;
        }
    }
}