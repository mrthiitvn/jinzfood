﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FRAMEWORK.Context.Common
{
    public class GlobalConstants
    {  
        ///Define role for user
        public const string ADMIN = "Admin";
        public const string MEMBER = "Member";
        public const string Moderator = "Moderator";

        /// not display, lock account 
        public const int STATUS_INACTIVE = 0;
        /// display, unlock account
        public const int STATUS_ACTIVE = 1;
        ///Status delete account
        public const int STATUS_DELETED = 2;

        public const string FLIGHTS_24h = "FLIGHTS_24h";
        public const string MOST_FLIGHTS = "MOST_FLIGHTS";
    }
}
