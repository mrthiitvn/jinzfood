﻿using FRAMEWORK.Context.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FRAMEWORK.Context.Common
{
    public class GlobalFunction
    {
        public static string RemoveHtmlTagsUsingRegex(string htmlString)
        {
            var result = Regex.Replace(htmlString, "&lt;.*?&gt;", string.Empty);
            return result;
        }
        //public static int getIdNVLogined()
        //{
        //    FRAMEWORK_CMSEntities context = new FRAMEWORK_CMSEntities();
        //    if (!string.IsNullOrEmpty(GlobalVariables.UserId))
        //    {
        //        int idUser = Int32.Parse(GlobalVariables.UserId);
        //        var dbNV = context.NHANVIENs.Where(x => x.UserId == idUser).FirstOrDefault();
        //        if (dbNV != null) return dbNV.NV_ID;
        //        else return 0;
        //    }
        //    return 0;
        //}
        //private ApplicationRoleManager _roleManager;
        //public ApplicationRoleManager RoleManager
        //{
        //    get
        //    {
        //        return _roleManager ?? Request.GetOwinContext().Get<ApplicationRoleManager>();
        //    }
        //    private set
        //    {
        //        _roleManager = value;
        //    }
        //}
        //public static Boolean isAdmin()
        //{
        //    var dsQuyen = RoleManager.Roles.ToList();
        //}
    }
}
