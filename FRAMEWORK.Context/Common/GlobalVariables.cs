﻿using System;
using System.Web;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace FRAMEWORK.Context.Common
{
    public class GlobalVariables
    {
        public static IPrincipal User
        {
            get
            {
                return HttpContext.Current.User;
            }
        }
        public static string Username
        {
            get
            {
                return User.Identity.IsAuthenticated ? User.Identity.Name : null;
            }
        }

        public static string UserId
        {
            get
            {
                return User.Identity.IsAuthenticated ? User.Identity.GetUserId<string>() : string.Empty;
            }
        }
        /// <summary>
        /// Trang thai isAdd = true nghia la dang them du lieu vao table, nguoc lai la chinh sua
        /// </summary>
        public static bool isAdd { get; set; }        
    }
}