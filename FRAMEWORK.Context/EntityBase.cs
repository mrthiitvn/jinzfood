﻿using FRAMEWORK.Context.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FRAMEWORK.Context
{    
    public partial class EntityBase
    {
        public EntityBase()
        {
            if (GlobalVariables.isAdd)
            {
                CreateDate = DateTime.Now;
            }
            LastEdit = DateTime.Now;
            LastEditBy = GlobalVariables.Username ?? GlobalConstants.ADMIN;
            Status = GlobalConstants.STATUS_ACTIVE;
        }
        public DateTime? CreateDate { get; set; }
        public DateTime? LastEdit { get; set; }
        public string LastEditBy { get; set; }
        public int? Status { get; set; }
    }
}
