﻿using FRAMEWORK.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FRAMEWORK.Context
{
    public partial class FRAMEWORK_CMSEntities : DbContext
    {
        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                //throw new Exception("loi tai day : " + ex.ToString());
                if (ex.EntityValidationErrors != null && ex.EntityValidationErrors.Count() > 0)
                {
                    string error = string.Join<string>("\r\n", ex.EntityValidationErrors.Select(x => JsonConvert.SerializeObject(new { Name = x.Entry.Entity.ToString(), x.ValidationErrors })));
                    throw new Exception(error);
                }
                else
                {
                    throw ex;
                }
            }

        }
    }
    public class IdentityContext : IdentityDbContext<ApplicationUser, ApplicationRole, int, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>
    {
        public IdentityContext()
            : base("IdentityContext")
        {
        }

        protected override void OnModelCreating(System.Data.Entity.DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            //Map Identity Model to Databse
            modelBuilder.Entity<ApplicationUser>().ToTable("AspNetUsers");
            modelBuilder.Entity<ApplicationUserRole>().ToTable("AspNetUserRoles");
            modelBuilder.Entity<ApplicationRole>().ToTable("AspNetRoles");
            //modelBuilder.Entity<ApplicationUserClaim>().ToTable("AspNetUserClaims");
            //modelBuilder.Entity<ApplicationUserLogin>().ToTable("AspNetUserLogins");
            //Set auto increase Id
            modelBuilder.Entity<ApplicationUser>().Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<ApplicationRole>().Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<ApplicationUserClaim>().Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }

        public static IdentityContext Create()
        {
            return new IdentityContext();
        }
    }

}
