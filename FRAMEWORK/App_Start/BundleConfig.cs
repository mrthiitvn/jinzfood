﻿using System.Web;
using System.Web.Optimization;

namespace FRAMEWORK
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            BundleTable.EnableOptimizations = false;

            #region --BEGIN BO--
            //ANGULARJS THƯ VIỆN CHUẨN
            bundles.Add(new ScriptBundle("~/angularjs-standard-library-bo").Include(                       
                        "~/Scripts/angular.js",//THU VIEN ANGULAR CHUAN
                        "~/Scripts/jquery-1.12.3.min.js",//THU VIEN JQUERY CHUAN
                        "~/Scripts/angular-route.min.js",//DUNG DE CHUYEN DUONG DAN
                        "~/Scripts/angular-cookies.min.js",//DUNG DE LUU COOKIE LUC DANG NHAP
                        "~/Scripts/angular-resource.min.js",//DUNG DE GOI API
                        "~/Scripts/angular-animate.js",//DUNG CHO CAC HIEU UNG TRONG ANGULAR
                        "~/FO/angular/loading-bar/loading-bar.js",//Hiển thị thanh loading
                //THU VIEN BOOTSTRAP 3
                        "~/Content/library/bootstrap-3/js/bootstrap.min.js",
                        "~/Content/library/bootstrap-3/js/angular-ui/ui-bootstrap.min.js",
                        "~/Content/library/bootstrap-3/js/angular-ui/ui-bootstrap-tpls.min.js",
                //HIEN THI THONG BAO
                        "~/Content/library/toastr/toastr.min.js", //HIEN THI THONG BAO
                        "~/Content/library/jquery-confirm/jquery-confirm.min.js",//THONG BAO XAC THUC, MODAL
                //CKEDITOR
                        "~/Content/library/ckeditor/ckeditor.js",
                //THANH LOADING NAM NGANG
                        "~/Content/library/loading-bar/loading-bar.js",
                        "~/Content/library/autocomplete/angucomplete.js",
                        //CUON TIEN ICH == ANCHOR
                        "~/Content/library/duScroll-anchor/angular-scroll.min.js", //CAN KHAI BAO duScroll 
                        "~/Content/library/time-ago/ngtimeago.js",//tinh thoi gian da dang bai viet
                        "~/Content/library/moment/moment.min.js",//dinh dang stringToDate trong API.serv
                        "~/Content/library/scroll/angular-scroll.min.js",
                        "~/Content/library/clipboard/angular-clipboard.js"//ho tro copy trong quan tri quang cao
                        ));

            bundles.Add(new StyleBundle("~/css-standard-library-bo").Include(
                        "~/Content/library/bootstrap-3/css/bootstrap.min.css",
                        "~/Content/library/bootstrap-3/css/bootstrap-theme.min.css",
                        "~/Content/library/bootstrap-3/css/ui-bootstrap/ui-bootstrap-csp.css",
                //HIEN THI THONG BAO
                        "~/Content/library/toastr/toastr.min.css",//HIEN THI THONG BAO
                        "~/Content/library/jquery-confirm/jquery-confirm.min.css",//THONG BAO XAC THUC, MODAL
                //THANH LOADING NAM NGANG
                        "~/Content/library/loading-bar/loading-bar.css",
                        "~/Content/library/autocomplete/angucomplete.css",
                        "~/FO/angular/loading-bar/loading-bar.css"//Hiển thị thanh loading
                        ));

            //JAVASCRIPT TEMPLATE ADMIN
            bundles.Add(new ScriptBundle("~/js-template-bo").Include(
                        "~/Content/library/sb-admin-2/js/metisMenu.min.js",//menu
                        "~/Content/library/sb-admin-2/js/sb-admin-2.js",
                        "~/Content/library/cropit/jquery.cropit.js" //Cat anh luc upload
                       ));
            //CSS TEMPLATE ADMIN
            bundles.Add(new ScriptBundle("~/css-template-bo").Include(
                        "~/Content/library/sb-admin-2/css/metisMenu.min.css",//menu
                        "~/Content/library/sb-admin-2/css/timeline.css",
                        "~/Content/library/sb-admin-2/css/sb-admin-2.css",
                        "~/Content/library/sb-admin-2/font-awesome/css/font-awesome.min.css" //font
                       ));

            #endregion --END BO--

            #region --BEGIN BROWSER--
            //ANGULARJS THƯ VIỆN CHUẨN
            bundles.Add(new ScriptBundle("~/angularjs-standard-library-browser").Include(
                        "~/Scripts/angular.min.js",//THU VIEN ANGULAR CHUAN
                        "~/Scripts/jquery-1.12.3.min.js",//THU VIEN JQUERY CHUAN
                        "~/Scripts/angular-route.min.js",//DUNG DE CHUYEN DUONG DAN
                        "~/Scripts/angular-cookies.min.js",//DUNG DE LUU COOKIE LUC DANG NHAP
                        "~/Scripts/angular-resource.min.js",//DUNG DE GOI API
                //THU VIEN BOOTSTRAP 3
                        "~/Content/library/bootstrap-3/js/bootstrap.min.js",
                        "~/Content/library/bootstrap-3/js/angular-ui/ui-bootstrap.min.js",
                        "~/Content/library/bootstrap-3/js/angular-ui/ui-bootstrap-tpls.min.js",
                //HIEN THI THONG BAO
                        "~/Content/library/toastr/toastr.min.js", //HIEN THI THONG BAO                
                //THANH LOADING NAM NGANG
                        "~/Content/library/loading-bar/loading-bar.js"               
                        ));
            bundles.Add(new StyleBundle("~/css-standard-library-browser").Include(
                        "~/Content/library/bootstrap-3/css/bootstrap.min.css",
                        "~/Content/library/bootstrap-3/css/bootstrap-theme.min.css",
                        "~/Content/library/bootstrap-3/css/ui-bootstrap/ui-bootstrap-csp.css",
                //HIEN THI THONG BAO
                        "~/Content/library/toastr/toastr.min.css",//HIEN THI THONG BAO
                //THANH LOADING NAM NGANG
                        "~/Content/library/loading-bar/loading-bar.css"
                        ));           
            #endregion --END BROWSER--

            #region --BEGIN AUTH--
            //ANGULARJS THƯ VIỆN CHUẨN
            bundles.Add(new ScriptBundle("~/angularjs-standard-library-auth").Include(
                        "~/Scripts/angular.min.js",//THU VIEN ANGULAR CHUAN
                        "~/Scripts/jquery-1.12.3.min.js",//THU VIEN JQUERY CHUAN
                        "~/Scripts/angular-route.min.js",//DUNG DE CHUYEN DUONG DAN
                        "~/Scripts/angular-cookies.min.js",//DUNG DE LUU COOKIE LUC DANG NHAP
                        "~/Scripts/angular-resource.min.js",//DUNG DE GOI API   
                        "~/Content/library/bootstrap-3/js/bootstrap.min.js",
                        "~/Content/library/bootstrap-3/js/angular-ui/ui-bootstrap.min.js",
                        "~/Content/library/bootstrap-3/js/angular-ui/ui-bootstrap-tpls.min.js",
                        "~/Content/library/toastr/toastr.min.js", //HIEN THI THONG BAO 
                        "~/Content/library/loading-bar/loading-bar.js" 
                        ));

            bundles.Add(new StyleBundle("~/css-standard-library-auth").Include(
                        "~/Content/library/bootstrap-3/css/bootstrap.min.css",
                        "~/Content/library/bootstrap-3/css/bootstrap-theme.min.css",
                        "~/Content/library/bootstrap-3/css/ui-bootstrap/ui-bootstrap-csp.css",
                        "~/Content/library/toastr/toastr.min.css",//HIEN THI THONG BAO
                        "~/Content/library/loading-bar/loading-bar.css",
                        "~/Content/library/sb-admin-2/font-awesome/css/font-awesome.min.css" //font
                        ));
            //LOGIN - AUTHENTICATION
            bundles.Add(new ScriptBundle("~/js-application-auth").Include(
                        "~/Content/authapp/authapp.js",
                        "~/Content/authapp/modules/*.module.js",
                        "~/Content/authapp/services/*.serv.js",
                        "~/Content/authapp/configs/*.conf.js",
                        "~/Content/authapp/controllers/*.ctrl.js",
                        "~/Content/authapp/js/script-custom.js",
                        "~/Content/authapp/js/angular-recaptcha.min.js"
                        ));
            bundles.Add(new StyleBundle("~/css-custom-auth").Include(
                        "~/Content/authapp/css/style-custom.css"
                      ));
            #endregion --BEGIN AUTH--

            #region FO Mobile

            bundles.Add(new ScriptBundle("~/fo-mobile-js").Include(
                        "~/FO/Mobile/js/jquery-3.2.1.js",
                        "~/FO/Mobile/js/jquery-migrate-3.0.0.js",
                        "~/FO/Mobile/js/slick/slick.min.js",
                        "~/FO/Mobile/js/hammer/hammer.min.js",
                        "~/FO/Mobile/js/hammer/hammer-time.min.js",
                        "~/FO/angular/angular.min.js",
                        "~/FO/angular/angular-touch.min.js",
                        "~/FO/angular/angular-sanitize.min.js",
                        "~/FO/angular/angular-animate.min.js",
                        "~/FO/angular/ng-hammer/angular.hammer.min.js",
                        "~/FO/angular/rzslider/rzslider.min.js",
                        "~/FO/angular/angular-locale_vi-vn.js",
                        "~/FO/angular/ui-bootstrap/ui-bootstrap-custom-2.5.0.min.js",
                        "~/FO/angular/ui-bootstrap/ui-bootstrap-custom-tpls-2.5.0.min.js",
                        "~/FO/angular/filter/angular-filter.js",
                        "~/Content/library/toastr/toastr.min.js",
                        "~/FO/angular/ui-scroll-lazy/ui-scroll.min.js",
                        "~/FO/angular/ui-scroll-lazy/ui-scroll-grid.min.js",
                        "~/FO/js/common.js",
                        "~/FO/frontend/Mobile.app.js",
                        "~/FO/frontend/directives/*.js",
                        "~/FO/frontend/services/*.js",
                        "~/FO/frontend/controller/*.ctrl.js",
                        "~/FO/frontend/filters/*.filter.js",
                        "~/FO/Mobile/js/main.js",
                        "~/FO/js/moment.min.js",
                        "~/FO/js/ngStorage.min.js",
                        "~/FO/js/sha256.min.js"
                        ));

            bundles.Add(new StyleBundle("~/fo-mobile-css").Include(
                        //"~/FO/angular/ui-bootstrap/css/bootstrap.min.css",
                        //"~/FO/angular/ui-bootstrap/css/bootstrap-theme.min.css",
                        "~/FO/Mobile/js/slick/slick.css",
                        "~/FO/Mobile/js/slick/slick-theme.css",
                        "~/FO/angular/rzslider/rzslider.min.css",
                        "~/Content/library/toastr/toastr.min.css",
                        "~/FO/fonts/font-awesome-4.7.0/css/font-awesome.min.css",
                        "~/FO/Mobile/css/animate.min.css",
                        "~/FO/Mobile/fonts/SegoeUI.css",
                        "~/FO/Mobile/css/c-hamburger.css",
                        "~/FO/Mobile/css/main.css"
                        ));
            #endregion
        }
    }
}
