﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Filters;

namespace FRAMEWORK
{
    /// <summary>
    /// The api controller exception filter attribute
    /// </summary>
    public class NotImplExceptionFilterAttribute : ExceptionFilterAttribute
    {
        private readonly ILog logger;
        public NotImplExceptionFilterAttribute(ILog _logger)
        {
            this.logger = _logger;
        }
        public override void OnException(HttpActionExecutedContext context)
        {
            context.Response = new HttpResponseMessage(HttpStatusCode.NotImplemented);
            if (this.logger != null)
            {
                logger.Error(context.Exception);
            }
        }
    }

    public abstract class MessageHandler : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var requestInfo = string.Format("{0};{1};{2}", request.Method, request.RequestUri.AbsolutePath, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));

            byte[] requestMessage = null;
            if (request.Content != null)
            {
                requestMessage = await request.Content.ReadAsByteArrayAsync();
            }
            var response = await base.SendAsync(request, cancellationToken);
            byte[] responseMessage = null;
            if (response.IsSuccessStatusCode && response.Content != null)
                responseMessage = await response.Content.ReadAsByteArrayAsync();
            else if (!string.IsNullOrEmpty(response.ReasonPhrase))
                responseMessage = Encoding.UTF8.GetBytes(response.ReasonPhrase);
            var responseInfo = string.Format("{0};{1};{2};{3}", request.Method, request.RequestUri.AbsolutePath, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), HttpContext.Current.User.Identity.Name);
            requestInfo += ";" + HttpContext.Current.User.Identity.Name;
            if (requestMessage != null)
            {
                await IncommingMessageAsync(requestInfo, requestMessage);
            }
            if (responseMessage != null)
            {
                await OutgoingMessageAsync(requestInfo, responseMessage);
            }
            return response;
        }


        protected abstract Task IncommingMessageAsync(string requestInfo, byte[] message);
        protected abstract Task OutgoingMessageAsync(string requestInfo, byte[] message);
    }

    public class MessageLoggingHandler : MessageHandler
    {
        private readonly ILog logger;
        public MessageLoggingHandler(ILog _logger)
        {
            this.logger = _logger;
        }
        protected override async Task IncommingMessageAsync(string requestInfo, byte[] message)
        {
            await Task.Run(() =>
            {
                if (logger != null)
                {
                    logger.Info(string.Format("Request: {0}\r\n{1}", requestInfo, message != null && message.Length < 5120 ? Encoding.UTF8.GetString(message) : "Request size : " + (message.Length / 1024) + "KB"));
                }
            });
        }


        protected override async Task OutgoingMessageAsync(string requestInfo, byte[] message)
        {
            await Task.Run(() =>
            {
                if (logger != null)
                {
                    logger.Info(string.Format("Response: {0}\r\n{1}", requestInfo, message != null && message.Length < 5120 ? Encoding.UTF8.GetString(message) : "Response size : " + (message.Length / 1024) + "KB"));
                }
            });
        }
    }

}