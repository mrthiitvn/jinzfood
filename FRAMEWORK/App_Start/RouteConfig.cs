﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FRAMEWORK
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                name: "FO_Template",
                url: "tpl/{entityName}/{templateName}",
                defaults: new { controller = "Template", action = "GetTemplate", templateName = UrlParameter.Optional, entityName = UrlParameter.Optional },
                namespaces: new[] { "FRAMEWORK.Controllers" }
            );
            routes.MapRoute(
                name: "Auth",
                url: "Auth",
                defaults: new { controller = "Template", action = "Auth" },
                namespaces: new[] { "FRAMEWORK.Controllers" }
            );
            routes.MapRoute(
                name: "SEO_URL",
                url: "ve-may-bay/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "FRAMEWORK.Controllers" }
            );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "FRAMEWORK.Controllers" }
            );
            routes.MapRoute(
                name: "DefaultUrl",
                url: "{*url}",
                defaults: new { controller = "Home", action = "Index" },
                namespaces: new[] { "FRAMEWORK.Controllers" }
           );
        }
    }
}
