﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using log4net;

namespace FRAMEWORK
{
    public static class WebApiConfig
    {
        public static ILog Logger { get; set; }
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            config.Filters.Add(new NotImplExceptionFilterAttribute(Logger));
            config.MessageHandlers.Add(new MessageLoggingHandler(Logger));
            // Cấu hình chuyển đổi kiểu định dạng chữ đầu là chữ thương như chuẩn javascript
            //config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            // Web API routes
            config.MapHttpAttributeRoutes();

            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);  

            config.Routes.MapHttpRoute(
                            name: "bo_ApiWithAction",
                            routeTemplate: "bo/api/{controller}/{action}/{id}",
                            defaults: new { id = RouteParameter.Optional, area = "bo" }
                        );
            config.Routes.MapHttpRoute(
              name: "bo_Api",
              routeTemplate: "bo/api/{controller}/{id}",
              defaults: new { id = RouteParameter.Optional, area = "bo" }
            );

            config.Routes.MapHttpRoute(
                name: "fo_ApiWithAction",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional, area = "Web" }
            );
            config.Routes.MapHttpRoute(
                name: "fo_Api",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional, area = "Web" }
            );
        }
    }
}
