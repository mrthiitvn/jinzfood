﻿(function () {
    'use strict';
    angular.module('authApp', ['common.core', 'common.ui']);
})();