﻿(function (app) {
    'use strict';
    app.config(config);
    config.$inject = ['$routeProvider', '$locationProvider'];
    function config($routeProvider, $locationProvider) {
        $routeProvider
            .otherwise({
                redirectTo: '/'
            });
        $locationProvider.html5Mode({ enabled: true, requireBase: false });
    }
})(angular.module('authApp'));

