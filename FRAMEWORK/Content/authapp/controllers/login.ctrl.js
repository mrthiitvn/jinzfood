﻿(function (app) {
    'use strict';

    app.controller('LoginCtrl', LoginCtrl);
    LoginCtrl.$inject = ['$http', '$scope', '$rootScope', 'GlobalServ', '$routeParams', 'AuthServ', '$location', 'MessageServ', '$cookies', 'ApiServ', 'vcRecaptchaService'];
    function LoginCtrl($http, $scope, $rootScope, GlobalServ, $routeParams, AuthServ, $location, MessageServ, $cookies, ApiServ, vcRecaptchaService) {
        var self = this;
        self.loginModel = {};
        self.publicKey = '';

        $scope.response = null;
        $scope.widgetId = null;

        //$scope.model = {
        //    key: '6LfqUyAUAAAAACOYytk6iZzbtt-TmnyctdQEtZ4A'
        //};
        $scope.model = {
            key: '6LerVVMUAAAAACIcrK7fiZLEQgNLsFwE5u1206mV'
        };

        $scope.setResponse = function (response) {
            console.info('Response available');

            $scope.response = response;
        };

        $scope.setWidgetId = function (widgetId) {
            console.info('Created widget ID: %s', widgetId);

            $scope.widgetId = widgetId;
        };

        $scope.cbExpiration = function () {
            console.info('Captcha expired. Resetting response object');

            vcRecaptchaService.reload($scope.widgetId);

            $scope.response = null;
        };


        this.signin = function () {
            //$http.post('/api/auth/confirmCaptcha', { recaptchaResponse: $scope.response })
            //.then(function (response) {
            //        if (response.data.success == true) {
            //            $cookies.put('userCode', self.loginModel.code);
            //            AuthServ.signin(self.loginModel,
            //                function () {
            //                    GlobalServ.refresh('/bo/');
            //                },
            //                function (error) {
            //                    if (error.data.error == 'invalid_user_lock') {                        
            //                        MessageServ.showWarning('Tài khoản đã bị khóa!\nVui lòng liên hệ với quản trị hệ thống để mở khóa!');
            //                    } else if (error.data.error == 'invalid_user_deleted') {                       
            //                        MessageServ.showError('Tài khoản này đã được xóa khỏi hệ thống !');
            //                    } else {
            //                        MessageServ.showError('Tên đăng nhập hoặc mật khẩu không đúng !');
            //                    }
            //                });
            //        }
            //        else if (response.data.ErrorCodes[0] == 'missing-input-response') {
            //            MessageServ.showWarning('Vui lòng chọn captcha.');
            //        }

            //    }, function () {
            //        // do on response failure
            //        MessageServ.showWarning('Xác nhận captcha thất bại.');
            //    });
            
            
            $cookies.put('userCode', self.loginModel.code);
            AuthServ.signin(self.loginModel,
                function () {
                    GlobalServ.refresh('/bo/');
                },
                function (error) {
                    if (error.data.error == 'invalid_user_lock') {                        
                        MessageServ.showWarning('Tài khoản đã bị khóa!\nVui lòng liên hệ với quản trị hệ thống để mở khóa!');
                    } else if (error.data.error == 'invalid_user_deleted') {                       
                        MessageServ.showError('Tài khoản này đã được xóa khỏi hệ thống !');
                    } else {
                        MessageServ.showError('Tên đăng nhập hoặc mật khẩu không đúng !');
                    }
                });
        }

    }
})(angular.module('authApp'));

