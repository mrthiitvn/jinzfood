﻿(function () {
    'use strict';
    angular.module('common.core', ['ngRoute', 'ngCookies', 'angular-loading-bar', 'vcRecaptcha']);
})()