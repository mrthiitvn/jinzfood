﻿(function (app) {
    'use strict';
    app.factory('AuthServ', AuthServ);
    AuthServ.$inject = ['GlobalServ', '$rootScope', '$cookies', 'ApiServ', '$httpParamSerializer', '$http', '$cookieStore', 'MessageServ'];
    function AuthServ(GlobalServ, $rootScope, $cookies, ApiServ, $httpParamSerializer, $http, $cookieStore, MessageServ) {
        var services = {
            signin: signin,
            signout: signout,
            fetchUserData: fetchUserData,
            isAuth: isAuth
        };

        function setAccessToken(token) {
            if (token != null) {
                $cookies.put('access_token', token, { path: '/' });
            }
            else {
                $cookies.remove('access_token', { path: '/' });
            }
        }

        function getAccessToken() {
            return $cookies.get('access_token', { path: '/' });
        }

        function setHeader(token) {
            if (token != null) {
                $http.defaults.headers.common['Authorization'] = 'Bearer ' + token;
            }
            else {
                $http.defaults.headers.common['Authorization'] = null;
            }
        }

        function signin(model, success, failure) {
            model.grant_type = 'password';
            ApiServ.post({
                url: '../api/auth/token', params: $httpParamSerializer(model),
                success: function (result) {
                    setAccessToken(result.access_token);
                    setHeader(result.access_token);
                    if (success != null) {
                        success();
                    }
                },
                failure: function (error) {
                    if (failure != null) {
                        failure(error);
                    }
                }
            });
        }

        function signout() {
            setAccessToken(null);
            setHeader(null);
            setUserData(null);
        }

        function fetchUserData(success, failure) {
            setHeader(getAccessToken());
            ApiServ.get({
                url: '../api/auth/me?code=' + $cookies.get('userCode'),
                success: function (result) {
                    if (result != null && result.Id > 0) {
                        setUserData(result);
                        if (success != null) {
                            success(result);
                        }
                    }
                    else {
                        //if (failure != null) {
                        //    failure();
                        //}
                        MessageServ.showError('Mã code nhập không đúng !');
                        setTimeout(function () {
                            signout();
                            GlobalServ.refresh("/auth");
                        }, 1500);
                        
                    }
                },
                failure: function (error) {
                    if (failure != null) {
                        failure();
                    }
                }
            });
        }

        function getUserData() {
            $rootScope.repository = $rootScope.repository || {};
            if ($rootScope.repository == null)
                $rootScope.repository = $cookieStore.get('objUserLogined');
            return $rootScope.repository.user;
        }

        function setUserData(user) {
            $rootScope.repository = $rootScope.repository || {};
            $rootScope.repository.user = user;
            if (user != null) $cookieStore.put('objUserLogined', user);
            else $cookieStore.remove('objUserLogined');
        }

        function isAuth(role) {
            if (role == null) {
                return getUserData() != null;
            }
            else {
                var user = getUserData();
                if (user != null && user.Roles != null) {
                    for (var i = 0; i < user.Roles.length ; i++) {
                        if (user.Roles[i].toLowerCase() == role.toLowerCase()) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        return services;
    }
})(angular.module('common.core'));