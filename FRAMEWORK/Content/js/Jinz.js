﻿// JavaScript source code
function animateBallToCart(e) {
    var o = $('<span class="qty-ball-fly-to-cart">1</span>');
    o.appendTo(document.body);
    var t = $(window).scrollTop(),
        n = e,
        a = n.offset(),
        i = n.width(),
        s = n.height(),
        c = $("#cart-qty"),
        r = c.offset(),
        l = {
            left: a.left + i / 2 - o.outerWidth(!0) / 2,
            top: a.top - t + s / 2 - o.outerHeight(!0) / 2
        },
        p = {
            left: r.left + c.width() / 2 - o.outerWidth(!0) / 2,
            top: r.top - t
        },
        d = {
            x: p.left - l.left,
            y: p.top - l.top
        };
    new TWEEN.Tween({
        x: 0,
        y: 0,
        old: {
            x: 0,
            y: 0
        }
    }).to({
        x: [.6 * d.x, d.x],
        y: [Math.min(-150, d.y - 100), d.y]
    }, 700).interpolation(TWEEN.Interpolation.Bezier).easing(TWEEN.Easing.Quadratic.Out).onUpdate(function () {
        o.css({
            left: this.x + l.left + "px",
            top: this.y + l.top + "px"
        }), this.old.x = this.x, this.old.y = this.y
    }).onComplete(function () {
        o.remove()
    }).start();
    animate()
}

function animate(e) {
    window.requestAnimationFrame(animate), TWEEN.update(e)
}

function starProductsNav() {
    $(".container-star-products .owl-item.center").on("transitionend", function (e) {
        var o = $(this),
            t = o[0].getBoundingClientRect(),
            n = $(window).width() > 1200 ? 45 : 30,
            a = t.x - n,
            i = a + t.width;
        $(".container-star-products .owl-nav .owl-prev").css({
            left: a + "px",
            top: "160px"
        }), $(".container-star-products .owl-nav .owl-next").css({
            left: i + "px",
            top: "160px"
        }), o.off(e)
    })
}

function initMap() {
    if (!map) {
        directionsService = new google.maps.DirectionsService, infowindow = new google.maps.InfoWindow, directionsDisplay = new google.maps.DirectionsRenderer({
            suppressMarkers: !0,
            polylineOptions: {
                strokeColor: "#d23c40"
            }
        });
        var e = {
            zoom: 17,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("places-map"), e), directionsDisplay.setMap(map), calcRoute()
    }
}

function calcRoute() {
    var e = new google.maps.LatLng(10.7773606, 106.6626088),
        o = new google.maps.LatLng(10.779968, 106.67772);
    createMarker(e, "img/start.png", "start"), createMarker(o, "img/end.png", "end");
    var t = {
        origin: e,
        destination: o,
        optimizeWaypoints: !0,
        travelMode: google.maps.DirectionsTravelMode.WALKING
    };
    directionsService.route(t, function (e, o) {
        if (o == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(e);
            e.routes[0]
        }
    })
}

function createMarker(e, o, t) {
    var n = new google.maps.Marker({
        position: e,
        title: t,
        map: map,
        icon: new google.maps.MarkerImage(o)
    });
    google.maps.event.addListener(n, "click", function () {
        infowindow.setContent(t), infowindow.open(map, n)
    })
}

function initMapPlaces() {
    mapPlacesAutocomplete || (mapPlacesAutocomplete = new google.maps.places.Autocomplete(document.getElementById("map-places-autocomplete"), {
        types: ["geocode"]
    })).addListener("place_changed", onPlaceChanged)
}

function onPlaceChanged() {
    var e = mapPlacesAutocomplete.getPlace();
    console.log(e)
}

function currentGeolocate() {
    navigator.geolocation && navigator.geolocation.getCurrentPosition(function (e) {
        var o = {
            lat: e.coords.latitude,
            lng: e.coords.longitude
        },
            t = new google.maps.Circle({
                center: o,
                radius: e.coords.accuracy
            });
        mapPlacesAutocomplete.setBounds(t.getBounds())
    })
}

function init() {
    starProductsNav()
}
window.requestAnimationFrame || (window.requestAnimationFrame = window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function (e, o) {
    window.setTimeout(e, 1e3 / 60)
});
var directionDisplay, directionsService, infowindow, map, mapPlacesAutocomplete;
$(document).ready(function (e) {
    "use strict";
    $('[data-toggle="offcanvas"]').on("click", function (o) {
        o.preventDefault();
        var t = e("body"),
            n = e(this).data("menu");
        e(n).addClass("active"), t.css("overflow", "hidden"), t.addClass("offcanvas-open")
    }), e(".site-backdrop").on("click", function () {
        var o = e("body");
        o.removeClass("offcanvas-open"), setTimeout(function () {
            o.css("overflow", "visible"), e(".offcanvas-container").removeClass("active")
        }, 450)
    });
    e(".offcanvas-menu .menu").height();
    e(".offcanvas-menu .offcanvas-submenu .back-btn").on("click", function (o) {
        var t = this,
            n = e(t).parent(),
            a = e(t).parent().parent().siblings().parent();
        e(t).parents(".menu");
        n.removeClass("in-view"), a.removeClass("off-view"), o.stopPropagation()
    }), e(".offcanvas-menu .has-children").on("click", function (o) {
        return e(this).parent("ul.menu").addClass("off-view"), e(this).children("ul.offcanvas-submenu").addClass("in-view"), o.preventDefault(), !1
    });
    var o = e(".scroll-to-top-btn");
    o.length > 0 && (e(window).on("scroll", function () {
        e(this).scrollTop() > 600 ? o.addClass("visible") : o.removeClass("visible")
    }), o.on("click", function (o) {
        o.preventDefault(), e("html").velocity("scroll", {
            offset: 0,
            duration: 1200,
            easing: "easeOutExpo",
            mobileHA: !1
        })
    })), e(document).on("click", ".scroll-to", function (o) {
        var t = e(this).attr("href");
        if ("#" === t) return !1;
        var n = e(t);
        if (n.length > 0) {
            var a = n.data("offset-top") || 70;
            e("html").velocity("scroll", {
                offset: e(this.hash).offset().top - a,
                duration: 1e3,
                easing: "easeOutExpo",
                mobileHA: !1
            })
        }
        o.preventDefault()
    }), e("body").on("click", ".product-card .icon-plus", function (o) {
        o.preventDefault(), animateBallToCart(e(this))
    }), $("#modal-booking").on("shown.bs.modal", function (e) {
        initMap()
    }), $("#modal-edit-shipping-info").on("shown.bs.modal", function () {
        initMapPlaces()
    }), init()
});