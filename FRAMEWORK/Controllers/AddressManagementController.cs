﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FRAMEWORK.Controllers
{
    public class AddressManagementController : Controller
    {
        // GET: AddressManagement
        public ActionResult Index()
        {
            return View();
        }

        // GET: AddressManagement/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: AddressManagement/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AddressManagement/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: AddressManagement/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: AddressManagement/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: AddressManagement/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: AddressManagement/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
