﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity.Owin;
using FRAMEWORK.Context;
using FRAMEWORK.Context.Identity;
using FRAMEWORK.Models.AUTH;
using FRAMEWORK.Data.Entity;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace FRAMEWORK.Controllers
{
    [RoutePrefix("Api/Auth")]
    public class AuthController : ApiController
    {        
        FRAMEWORK_CMSEntities context = new FRAMEWORK_CMSEntities();
        public static string _ngonngu = "vi";

        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [ActionName("Me"), HttpGet]
        public UserDataViewModel GetUserData(string code = "")
        {
            if (User.Identity.IsAuthenticated)
            {
                var dbUser = UserManager.FindById(User.Identity.GetUserId<int>());
                if(dbUser.Code != code)
                {
                    return null;
                }
                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<ApplicationUser, UserDataViewModel>()
                        .ForMember(d => d.Roles, src => src.MapFrom(x => x.Roles.ToList().Select(role => role.Role.Name).ToList()));                       
                });
                IMapper mapper = config.CreateMapper();
                var source = dbUser;
                var result = mapper.Map<UserDataViewModel>(source);
                return result;
            }
            return null;
        }

        //[HttpPost]
        //[Route("confirmCaptcha")]
        //public async Task<RecaptchaVerifyResponse> Post([FromBody] modelDataCaptcha model)
        //{

        //    if (!ModelState.IsValid) return null;

        //    var remoteIp = Request.GetOwinContext().Request.RemoteIpAddress;
        //    var recaptchaVerifyResponse = await VerifyAsync(model.RecaptchaResponse, remoteIp);

        //    //if (recaptchaVerifyResponse == null || !recaptchaVerifyResponse.Success) return null;

        //    return recaptchaVerifyResponse;
        //}

        //private async Task<RecaptchaVerifyResponse> VerifyAsync(string response, string remoteip)
        //{
        //    const string RecaptchaVerifyUrl = "https://www.google.com/recaptcha/api/siteverify";

        //    string responseString;
        //    using (var client = new HttpClient())
        //    {
        //        var values = new Dictionary<string, string>
        //    {
        //        //{"secret", "6LfqUyAUAAAAAKrOYCDbj8LHggz1Kqm9yBQHRYn4"},
        //        {"secret", "6LerVVMUAAAAABpod7dci8ZpSoinLuJXfkVyvyJD"},
        //        {"response", response},
        //        {"remoteip", remoteip},
        //    };

        //        var content = new FormUrlEncodedContent(values);
        //        var postResponse = await client.PostAsync(RecaptchaVerifyUrl, content);
        //        responseString = await postResponse.Content.ReadAsStringAsync();
        //    }

        //    if (string.IsNullOrWhiteSpace(responseString)) return null;

        //    RecaptchaVerifyResponse result;
        //    try
        //    {
        //        result = JsonConvert.DeserializeObject<RecaptchaVerifyResponse>(responseString);
        //    }
        //    catch (JsonException)
        //    {
        //        return null;
        //    }

        //    return result;
        //}
    }

    public class modelDataCaptcha
    {
        public string RecaptchaResponse { get; set; }
    }

    

    public class RecaptchaVerifyResponse
    {
        [JsonProperty("success")]
        private bool? _success = null;

        public bool Success
        {
            get { return _success == true; }
        }

        [JsonProperty("error-codes")]
        private string[] _errorCodes = null;

        public string[] ErrorCodes
        {
            get { return _errorCodes ?? new string[0]; }
        }
    }
}
