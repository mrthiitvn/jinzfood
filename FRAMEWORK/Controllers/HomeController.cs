﻿using AutoMapper;
using FRAMEWORK.Context;
using FRAMEWORK.Context.Common;
using FRAMEWORK.Models.FO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Configuration;

using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Security.Cryptography;
using System.Text;
using System.Net;
using System.IO;
using RestSharp;
using System.Web.Script.Serialization;
using System.Web;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FRAMEWORK.Controllers
{

    public class HomeController : Controller
    {
        FRAMEWORK_CMSEntities context = new FRAMEWORK_CMSEntities();
        MapperConfiguration config = new MapperConfiguration(cfg =>
        {
            //cfg.CreateMap<Configuration, FOConfigurationModel>();
            //cfg.CreateMap<FOConfigurationModel, Configuration>();
        });
        public string ReplaceAt(string str, int index, int length, string replace)
        {
            return str.Remove(index, Math.Min(length, str.Length - index))
                    .Insert(index, replace);
        }
        public ActionResult Index()
        {
            ViewBag.Description = "Jinzfood";
            ViewBag.Title = "Jinzfood";
            ViewBag.Author = "Jinzfood";
            ViewBag.Keywords = "Jinzfood";
            ViewBag.CanonicalURL = "http://www.jinzfood.com";
            //SEO URL FB,G+
            ViewBag.OgType = "article";
            ViewBag.OgUrl = "http://www.jinzfood.com";
            ViewBag.OgTitle = ViewBag.Title;
            ViewBag.OgDescription = ViewBag.Description;
            ViewBag.OgImageUrl = "http://www.jinzfood.com";
            ViewBag.OgImageWidth = "211";
            ViewBag.OgImageHeight = "128";
            //
            return View();
        }
       
        

        public ActionResult Details()
        {
            return View();
        }       
    }
}





        

       
