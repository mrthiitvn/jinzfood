﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FRAMEWORK.Controllers
{
    public class OrderTrackingController : Controller
    {
        // GET: OrderTracking
        public ActionResult Index()
        {
            return View();
        }

        // GET: OrderTracking/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: OrderTracking/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: OrderTracking/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: OrderTracking/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: OrderTracking/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: OrderTracking/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: OrderTracking/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
