﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
namespace FRAMEWORK.Controllers
{
    public class TemplateController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Auth()
        {
            return PartialView("Auth/Index");
        }

        public ActionResult GetTemplate(string entityName, string templateName)
        {
            if (string.IsNullOrEmpty(templateName)) templateName = "Index";
            return PartialView(entityName + "/" + templateName);
        }
    }
}
