﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FRAMEWORK.Controllers
{
    public class UpdateVATInfoController : Controller
    {
        // GET: UpdateVATInfo
        public ActionResult Index()
        {
            return View();
        }

        // GET: UpdateVATInfo/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: UpdateVATInfo/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UpdateVATInfo/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: UpdateVATInfo/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: UpdateVATInfo/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: UpdateVATInfo/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: UpdateVATInfo/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
