//detect facebook app browser
function isFacebookApp() {
    var ua = navigator.userAgent || navigator.vendor || window.opera;
    return (ua.indexOf("FBAN") > -1) || (ua.indexOf("FBAV") > -1);
}
function isIOS() {
    var ua = navigator.userAgent || navigator.vendor || window.opera;
    return /iPad|iPhone|iPod/.test(ua) && !window.MSStream;
}
function adaptCSSFbBrowser() {
    var ua = navigator.userAgent || navigator.vendor || window.opera;
    if (isFacebookApp(ua) && isIOS()) { // Facebook in-app browser detected
        $("#SelectedFlightContainer ").css("bottom", "48px");
    }
};

$(function () {
	//prevent scaling ios 10
	//document.addEventListener('touchmove', function (event) {
	//	if (event.scale !== 1) {event.scale=1; event.preventDefault(); }
	//}, false);
    //$('.c-hamburger').click(function () {
    //    $(this).hasClass('is-active') ? $(this).removeClass('is-active') : $(this).addClass('is-active');
    //});
    //$('#test').click(function () {
    //    $('body').hasClass('modal-open') ? $('body').removeClass('modal-open') : $('body').addClass('modal-open');
    //    $(this).text($('body').hasClass('modal-open'));
    //});
    $('#toogle-menu-1').click(function () {
        $(this).hasClass('is-active') ? $(this).removeClass('is-active') : $(this).addClass('is-active');
        $('#toogle-menu-2').hasClass('is-active') ? $('#toogle-menu-2').removeClass('is-active') : $('#toogle-menu-2').addClass('is-active');
        $('#mMainMenu').hasClass('is-active') ? $('#mMainMenu').removeClass('is-active') : $('#mMainMenu').addClass('is-active');
        $('#underlay').hasClass('is-active') ? $('#underlay').removeClass('is-active') : $('#underlay').addClass('is-active');
    });
    $('#toogle-menu-2').click(function () {
        $('#toogle-menu-1').click();
    });
    $('#services-slider').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true
    });
    $('#logo-carousel').slick({
        dots: true,
        infinite: true,
        autoplay: true,
        speed: 300,
        slidesToShow: 4,
        centerMode: true,
        variableWidth: true,
        arrows: false
    });
    $('#banks').slick({
        dots: false,
        infinite: false,
        autoplay: false,
        speed: 300,
        slidesToShow: 2,
        centerMode: false,
        variableWidth: false,
        arrows: true
    });
    var hammertime = new Hammer(document.getElementById('mMainMenu'));
    hammertime.on('swipeleft', function (ev) {
        $('#toogle-menu-1').click();
    });


    //fix facebook size
    // Viewport height hack for Facebook app browser
    //window.addEventListener('resize', function () {
    //    onResize();
    //});
    //function onResize() {
    //    document.querySelector('html').style.height = window.innerHeight + 'px'
    //};
    //onResize();
    function _fixViewportHeight() {
        var html = document.querySelector('html');

        function _onResize(event) {
            html.style.height = window.innerHeight + 'px';
        }

        window.addEventListener('resize', _.debounce(_onResize, 125, {
            leading: true,
            maxWait: 250,
            trailing: true
        }));
        _onResize();
    }
    _fixViewportHeight();

    //test scroll
    $(".wrapper").scroll(function () {
        //console.log($("body").scrollTop() + "," + window.pageYOffset + "," + $(".wrapper:first").scrollTop() + "," + $(document).scrollTop());
    });
});
