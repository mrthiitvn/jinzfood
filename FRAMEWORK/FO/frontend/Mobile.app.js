﻿'use strict';
var app = angular.module('myapp', ['ngTouch', 'ngSanitize', 'ui.bootstrap', 'angular.filter', 'hmTouchEvents', 'rzModule', 'ngStorage', 'angular-loading-bar', 'hl.sticky', 'ui.scroll', 'ui.scroll.grid']);

//Tắt vòng xoay tròn trên thanh loading bar
app.config(['cfpLoadingBarProvider', function (cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = true;
    cfpLoadingBarProvider.includeSpinner = true;
}]);
//Dời thanh loading bar vào bên dưới trang
app.config(['cfpLoadingBarProvider', function (cfpLoadingBarProvider) {
    cfpLoadingBarProvider.parentSelector = '#loading-bar-container';
    cfpLoadingBarProvider.spinnerTemplate = '<div id="loading-bar-spinner">Đang lấy tải dữ liệu... (<span></span>)</div>';
}]);

app.directive('isolateScrolling', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            var oldY = 0;
            element.on('DOMMouseScroll', function (ev) {
                var $this = $(this),
                    scrollTop = this.scrollTop,
                    scrollHeight = this.scrollHeight,
                    height = $this.innerHeight(),
                    delta = (ev.type == 'DOMMouseScroll' ?
                        ev.originalEvent.detail * -40 :
                        ev.originalEvent.wheelDelta),
                    up = delta > 0;

                var prevent = function () {
                    ev.stopPropagation();
                    ev.preventDefault();
                    ev.returnValue = false;
                    return false;
                }

                if (!up && -delta > scrollHeight - height - scrollTop) {
                    // Scrolling down, but this will take us past the bottom.
                    $this.scrollTop(scrollHeight);
                    return prevent();
                } else if (up && delta > scrollTop) {
                    // Scrolling up, but this will take us past the top.
                    $this.scrollTop(0);
                    return prevent();
                }
            });
            element.on('mousewheel', function (ev) {
                var $this = $(this),
                    scrollTop = this.scrollTop,
                    scrollHeight = this.scrollHeight,
                    height = $this.innerHeight(),
                    delta = (ev.type == 'DOMMouseScroll' ?
                        ev.originalEvent.detail * -40 :
                        ev.originalEvent.wheelDelta),
                    up = delta > 0;

                var prevent = function () {
                    ev.stopPropagation();
                    ev.preventDefault();
                    ev.returnValue = false;
                    return false;
                }

                if (!up && -delta > scrollHeight - height - scrollTop) {
                    // Scrolling down, but this will take us past the bottom.
                    $this.scrollTop(scrollHeight);
                    return prevent();
                } else if (up && delta > scrollTop) {
                    // Scrolling up, but this will take us past the top.
                    $this.scrollTop(0);
                    return prevent();
                }
            });
            element.on('touchmove', function (ev) {
                //console.log(ev);
                var $this = $(this),
                    scrollTop = this.scrollTop,
                    scrollHeight = this.scrollHeight,
                    height = $this.innerHeight();
                var prevent = function () {
                    ev.preventDefault();
                    //ev.stopPropagation();
                    //ev.returnValue = false;
                    //return false;
                }
                var up = ev.originalEvent.changedTouches[0].clientY > oldY;
                oldY = ev.originalEvent.changedTouches[0].clientY;
                //console.log([(scrollTop + height >= scrollHeight) && !up, scrollTop, scrollHeight, height]);
                if (scrollTop == 0 && up) return prevent();
                if ((scrollTop + height >= scrollHeight) && !up) return prevent();
            });
        }
    };
});

app.directive('stopTouchPropagation', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            element.bind('touchmove', function (ev) {
                //ev.stopPropagation();
                return true;
            });
        }
    }
});
//Dời thanh loading bar vào bên dưới trang
//app.config(['cfpLoadingBarProvider', function (cfpLoadingBarProvider) {
//    cfpLoadingBarProvider.parentSelector = '#loading-bar-container';
//    cfpLoadingBarProvider.spinnerTemplate = '<div id="loading-bar-spinner">Đang lấy thông tin chuyến bay... (<span></span>)</div>';
//}]);
app.service('anchorSmoothScroll', function () {

    this.scrollTo = function (eID, offset) {
        if (offset == null) offset = 0;
        var startY = currentYPosition();
        var stopY = elmYPosition(eID) + offset;
        var distance = stopY > startY ? stopY - startY : startY - stopY;
        if (distance < 100) {
            scrollTo(0, stopY); return;
        }
        var speed = Math.round(distance / 100);
        if (speed >= 20) speed = 20;
        var step = Math.round(distance / 25);
        var leapY = stopY > startY ? startY + step : startY - step;
        var timer = 0;
        if (stopY > startY) {
            for (var i = startY; i < stopY; i += step) {
                setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
                leapY += step; if (leapY > stopY) leapY = stopY; timer++;
            } return;
        }
        for (var i = startY; i > stopY; i -= step) {
            setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
            leapY -= step; if (leapY < stopY) leapY = stopY; timer++;
        }

        function currentYPosition() {
            // Firefox, Chrome, Opera, Safari
            if (self.pageYOffset) return self.pageYOffset;
            // Internet Explorer 6 - standards mode
            if (document.documentElement && document.documentElement.scrollTop)
                return document.documentElement.scrollTop;
            // Internet Explorer 6, 7 and 8
            if (document.body.scrollTop) return document.body.scrollTop;
            return 0;
        }

        function elmYPosition(eID) {
            var elm = document.getElementById(eID);
            var y = elm.offsetTop;
            var node = elm;
            while (node.offsetParent && node.offsetParent != document.body) {
                node = node.offsetParent;
                y += node.offsetTop;
            } return y;
        }
    };
});