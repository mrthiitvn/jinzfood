﻿(function (app) {
    'use strict';
    app.controller('AccountInfoController', AccountInfoController);
    AccountInfoController.$inject = ['MessageServ', 'ApiServ', '$rootScope', '$scope', '$location', 'anchorSmoothScroll', '$filter', '$window', '$sessionStorage', '$uibModal', '$http'];
    function AccountInfoController(MessageServ, ApiServ, $rootScope, $scope, $location, anchorSmoothScroll, $filter, $window, $sessionStorage, $uibModal, $http) {
        var baseApiUrl = "http://beta.jinz.vn";
        $scope.baseImageUrl = 'http://beta.jinz.vn/images/';
        $scope.accountInfo = {};
        $scope.oldPassword = "";
        $scope.newPassword = "";
        $scope.prePassword = "";
        this.OnInit = function () {
            $scope.GetAccountInfo();
        }
        $scope.GetAccountInfo = function () {
            var url = baseApiUrl + "/api/customer/hre@cofd.com";
            ApiServ.get({
                url: url,
                success: function (result) {
                    if (result) {
                        console.log("Lấy thông tin thành công.", result);
                        $scope.accountInfo = result[0];
                    }
                },
                failure: function (error) {
                    console.log("Lấy thông tin thất bại.")
                }
            });
            //$scope.accountInfo = {
            //    id: 0,
            //    username: "hre@cofd.com",
            //    firstName: "Ronaldo",
            //    address: "Tp.Hồ Chí Minh",
            //    lastName: "Cristiano",
            //    email: "hre@cofd.com",
            //    phone: "0901199199",
            //    password: "krWHEN1FONaw58JgDYMQJA==",
            //    gioiTinh: 0
            //}
        }
        $scope.Save = function () {
            var url = baseApiUrl + '/account/updateCustomer';
            ApiServ.post({
                url: url,
                params: $scope.accountInfo,
                success: function (result) {
                    if (result) {
                        //console.log("Cập nhật thông tin thành công.")
                        $('#open-modal-update-info-success').trigger('click');
                    }
                },
                failure: function (error) {
                    $('#open-modal-update-info-error').trigger('click');
                    //console.log("Cập nhật thông tin thất bại.")
                }
            });
            
        }

        $scope.ChangePassword = function () {
            var url = baseApiUrl + "/account/updatePassword";
            $scope.accountInfo.password = $scope.newPassword;
            ApiServ.post({
                url: url,
                params: $scope.accountInfo,
                success: function (result) {
                    if (result) {
                        //console.log("Cập nhật mật khẩu thành công.")
                        $('#open-modal-change-password-success').trigger('click');
                    }
                },
                failure: function (error) {
                    //console.log("Cập nhật mật khẩu thất bại.")
                    $('#open-modal-change-password-success').trigger('click');
                }
            });
        }
        this.OnInit();
    };
})(angular.module('myapp'));
