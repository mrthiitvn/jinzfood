
(function (app) {
    'use strict';
    app.controller('ContactController', ContactController);
    ContactController.$inject = ['MessageServ', 'ApiServ', '$rootScope', '$scope', '$sce','$http'];
    function ContactController(MessageServ, ApiServ, $rootScope, $scope, $sce, $http) {
        $rootScope.trustHtml = function (html) {
            return $sce.trustAsHtml(html);
        }
        $scope.modelContact = {};
        this.load = function () {
            $scope.SetDefaultContact();
        }

        $scope.SetDefaultContact = function () {
            $scope.modelContact = {
                fullName: "",
                phone: "",
                email: "",
                message: ""
            }
        }
        //Create Contact
        $scope.SendContact = function (modelContact) {
            console.log(modelContact);
            ApiServ.post({
                url: 'http://beta.jinz.vn/contact/SendRequest',
                params: modelContact,
                success: function (result) {
                    if (result == 'Success') {
                        modelContact = null;
                        //MessageServ.showSuccess('Gửi thông tin thành công !');
                        //return;
                        $scope.SetDefaultContact();
                        $('#open-modal-send-contact-success').trigger('click');
                    } else {
                        //MessageServ.showError('Đã có lỗi xảy ra');
                        //return;
                        $('#open-modal-send-contact-error').trigger('click');
                    }
                },
                failure: function (error) {
                    //MessageServ.showError('Gửi thông tin bị lỗi !');
                    $('#open-modal-send-contact-error').trigger('click');
                }
            });
        }
        this.load();
    };
})(angular.module('myapp'));
