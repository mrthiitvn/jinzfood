﻿
(function (app) {
    'use strict';
    app.controller('HomeController', HomeController);
    HomeController.$inject = ['MessageServ', 'ApiServ', '$rootScope', '$scope', '$location', 'anchorSmoothScroll', '$filter', '$window', '$sessionStorage', '$uibModal', '$http', '$timeout', 'NgMap'];

    function initMap() {
        if (!map) {
            directionsService = new google.maps.DirectionsService, infowindow = new google.maps.InfoWindow, directionsDisplay = new google.maps.DirectionsRenderer({
                suppressMarkers: !0,
                polylineOptions: {
                    strokeColor: "#d23c40"
                }
            });
            var e = {
                zoom: 17,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("places-map"), e), directionsDisplay.setMap(map), calcRoute()
        }
    }

    function calcRoute() {
        var e = new google.maps.LatLng(10.7773606, 106.6626088),
            o = new google.maps.LatLng(10.779968, 106.67772);
        createMarker(e, "img/start.png", "start"), createMarker(o, "img/end.png", "end");
        var t = {
            origin: e,
            destination: o,
            optimizeWaypoints: !0,
            travelMode: google.maps.DirectionsTravelMode.WALKING
        };
        directionsService.route(t, function (e, o) {
            if (o == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(e);
                e.routes[0]
            }
        })
    }

    function createMarker(e, o, t) {
        var n = new google.maps.Marker({
            position: e,
            title: t,
            map: map,
            icon: new google.maps.MarkerImage(o)
        });
        google.maps.event.addListener(n, "click", function () {
            infowindow.setContent(t), infowindow.open(map, n)
        })
    }

 



  
    function HomeController(MessageServ, ApiServ, $rootScope, $scope, $location, anchorSmoothScroll, $filter, $window, $sessionStorage, $uibModal, $http, $timeout, NgMap, $localStorage) {
        var baseApiUrl = "http://beta.jinz.vn";
        $scope.baseImageUrl = 'http://beta.jinz.vn/images/';
        $scope.productPrograms = [];
        $scope.productFeatures = [];
        $scope.locationList = [];
        $scope.location = {};
        $scope.groupProductFeatures = [];
        $scope.productOrderNow = [];
        $scope.order = {};
        $scope.infoAddress = {};
        $scope.locationDefault = "";
        $scope.selectedGroup = 0;
        $scope.shippingInfo = {};
        $scope.hotProductTab1 = [];
        $scope.hotProductTab2 = [];
        $scope.hotProductTab3 = [];
        $scope.hotProductTab4 = [];
        $scope.searchValue = "";
        $scope.listSearchProduct = [];
        $scope.totalItem = 0;
        $scope.lat = undefined;
        $scope.lng = undefined;

       
        

        

       
        //Autocomplete
 
        $scope.types = "['address']";
        $scope.placeChanged = function () {
            $scope.place = this.getPlace();
            console.log('location', $scope.place.geometry.location);
            $scope.map.setCenter($scope.place.geometry.location);
        }
      
        
        
        $scope.session = null;
        this.OnInit = function () {

           

            if ($sessionStorage.session) {
                $scope.session = $sessionStorage.session;
            }
            //else {
            //    $window.location.href = '/login';
            //}

            $scope.GetProductProgram();
            $scope.GetGroupProductFeature();
            $scope.GetProductFeatureTab1();
            $scope.GetProductFeatureTab2();
            $scope.GetProductFeatureTab3();
            $scope.GetProductFeatureTab4();
            $scope.GetProductOrderNow();
            $scope.GetLocations();
            $scope.GetLocation();
            $scope.InitCard();
            $scope.SetDefaultShippingInfo();
            $scope.paging = { maxSize: 9, itemsPerPage: 12, current: 1 };//paging
            var path = $location.absUrl();
            console.log("path", path);
            if ($sessionStorage.searchValue && path.indexOf("/SearchProduct") !== -1) {
                $scope.searchValue = $sessionStorage.searchValue;
            }
            else {
                $sessionStorage.searchValue = "";
                $scope.searchValue = "";
                $("#searchValue").val("");
            }
            $scope.GetProducts();
            $scope.SetViewMap(null, null);
        }



        //Map
        $scope.SetViewMap = function (destinationLat, destinationLong) {
            
            $scope.originLat = 10.7554061;
            $scope.originLong = 106.6903797;
            $scope.destinationLat = destinationLat != null ? destinationLat : 10.795070;
            $scope.destinationLong = destinationLong != null ? destinationLong : 106.596009;
            $scope.center = [$scope.originLat, $scope.originLong];
            $scope.positions = [{ pos: [$scope.originLat, $scope.originLong], name: "User" }, { pos: [$scope.destinationLat, $scope.destinationLong], name: "Store" }];
            //$scope.viewMap = true;
            $scope.path = [[$scope.originLat, $scope.originLong], [$scope.destinationLat, $scope.destinationLong]];
        }


        //Sản phẩm nổi bậc
        $scope.GetProductFeatureTab1 = function () {
            var url = baseApiUrl + "/productprofile/getproductfeature/" + 7;
            ApiServ.get({
                url: url,
                success: function (results) {
                    console.log("Sản phẩm nổi bậc", results);
                    if (results.length > 0) {
                        //$scope.productFeatures = results;
                        $scope.hotProductTab1 = results
                        
                        
                    }
                },
                failure: function (error) {
                    MessageServ.showError('Lấy danh sách sản phẩm nổi bậc không thành công !');
                }
            });
        }
        //Sản phẩm nổi bậc
        $scope.GetProductFeatureTab2 = function () {
            var url = baseApiUrl + "/productprofile/getproductfeature/" + 21;
            ApiServ.get({
                url: url,
                success: function (results) {
                    console.log("Sản phẩm nổi bậc", results);
                    if (results.length > 0) {
                        $scope.hotProductTab2 = results
                       
                    }
                },
                failure: function (error) {
                    MessageServ.showError('Lấy danh sách sản phẩm nổi bậc không thành công !');
                }
            });
        }
        //Sản phẩm nổi bậc
        $scope.GetProductFeatureTab3 = function () {
            var url = baseApiUrl + "/productprofile/getproductfeature/" + 26;
            ApiServ.get({
                url: url,
                success: function (results) {
                    console.log("Sản phẩm nổi bậc", results);
                    if (results.length > 0) {
                        $scope.hotProductTab3 = results
                    }
                },
                failure: function (error) {
                    MessageServ.showError('Lấy danh sách sản phẩm nổi bậc không thành công !');
                }
            });
        }
        //Sản phẩm nổi bậc
        $scope.GetProductFeatureTab4 = function () {

            var url = baseApiUrl + "/productprofile/getproductfeature/" + 30;
            ApiServ.get({
                url: url,
                success: function (results) {
                    console.log("Sản phẩm nổi bậc", results);
                    if (results.length > 0) {
                        $scope.hotProductTab4 = results
                    }
                },
                failure: function (error) {
                    MessageServ.showError('Lấy danh sách sản phẩm nổi bậc không thành công !');
                }
            });
        }

        

        //Sản phẩm nổi bậc
        $scope.GetProductFeature = function (groupId) {
            var url = baseApiUrl + "/productprofile/getproductfeature/" + groupId;
            ApiServ.get({
                url: url,
                success: function (results) {
                    console.log("Sản phẩm nổi bậc", results);
                    if (results.length > 0) {
                       
                        return results;
                    }
                },
                failure: function (error) {
                    MessageServ.showError('Lấy danh sách sản phẩm nổi bậc không thành công !');
                }
            });
        }


        //Sản phẩm bán chạy
        $scope.GetProductProgram = function () {
            var url = baseApiUrl + "/api/productprogram/1";
            ApiServ.get({
                url: url,
                success: function (results) {
                    console.log("Sản phẩm bán chạy", results);
                    if (results.length > 0) {
                        $scope.productPrograms = results;
                        $timeout(function () {
                            $(".product.slide").owlCarousel({
                                items: 5,
                                dots: false,
                                nav: true,
                                navText: ['', ''],
                                lazyLoad: true,
                                responsiveClass: true,
                                autoPlay: true,
                                center: true,
                                loop: true,
                                margin: 30,

                                responsive: {
                                    0: { items: 1 }, 576: { items: 2 }, 768: { items: 3 }, 991: { items: 4 }, 1200: { items: 4 }
                                },
                                onInitialized: function () {
                                    $('.product-slide-header').show();
                                },
                            });
                        }, 2000);
                    }
                },
                failure: function (error) {
                    MessageServ.showError('Lấy danh sách sản phẩm bán chạy không thành công !');
                }
            });
        }

        //Group Sản phẩm nổi bậc
        $scope.GetGroupProductFeature = function () {
            var url = baseApiUrl + "/api/group";
            ApiServ.get({
                url: url,
                success: function (results) {
                    if (results.length > 0) {
                        $scope.groupProductFeatures = results;
                        $scope.selectedGroup = results[0].id;
                        //$scope.GetProductFeature($scope.selectedGroup);
                    }
                },
                failure: function (error) {
                    MessageServ.showError('Lấy danh sách group sản phẩm nổi bậc không thành công !');
                }
            });
        }
        $scope.ChangeGroupProduct = function (group) {
            $scope.selectedGroup = group.id;
            $scope.productFeatures = [];
            
        }

        //List product order now
        $scope.GetProductOrderNow = function () {
            var orderDate = new Date().toISOString().substr(0, 10);
            console.log(orderDate);
            var url = baseApiUrl + "/api/ProductSearch/GetProductOrderNow/" + orderDate;
            ApiServ.get({
                url: url,
                success: function (results) {
                    if (results.length > 0) {
                        $scope.productOrderNow = results;
                        $timeout(function () {
                            $(".product.slide").owlCarousel({
                                items: 5,
                                dots: false,
                                nav: true,
                                navText: ['', ''],
                                lazyLoad: true,
                                responsiveClass: true,
                                autoPlay: true,
                                center: true,
                                loop: true,
                                margin: 30,

                                responsive: {
                                    0: { items: 1 }, 576: { items: 2 }, 768: { items: 3 }, 991: { items: 4 }, 1200: { items: 4 }
                                },
                                onInitialized: function () {
                                    $('.product-slide-header').show();
                                },
                            });
                        }, 2000);
                    }
                },
                failure: function (error) {
                    MessageServ.showError('Lấy danh sách group sản phẩm nổi bậc không thành công !');
                }
            });
        }
        //beta.jinz.vn/api/ProductSearch/GetProductOrderNow/2018-04-13
        //Danh sách location
        $scope.GetLocations = function () {
            var url = baseApiUrl + "/api/brand";
            ApiServ.get({
                url: url,
                success: function (results) {
                    console.log("Danh sách location", results);
                    if (results.length > 0) {
                        $scope.locationList = results;
                        $scope.locationDefault = $scope.locationList[0].name;
                    }
                },
                failure: function (error) {
                    MessageServ.showError('Lấy danh sách location không thành công !');
                }
            });
        }
        //Get one location
        $scope.GetLocation = function () {
            var url = baseApiUrl + "/api/brand/jinzfood-ben-van-don";
            ApiServ.get({
                url: url,
                success: function (result) {
                    console.log("Get one location", result);
                    if (result) {
                        $scope.location = result;
                    }
                },
                failure: function (error) {
                    MessageServ.showError('Lấy location không thành công !');
                }
            });
        }
        //Init Card
        $scope.InitCard = function () {
            if ($sessionStorage.order) {
                $scope.order = $sessionStorage.order;
            }
            else {
                $scope.SetDefaultOrder();
            }
            $scope.CalculateCard();
        }

        $scope.SetDefaultOrder = function () {
            $scope.order = {
                userId: $scope.session ? $scope.session.id : 0,
                orderId: 0,
                orderAddress: "",
                orderDistance: 0,
                orderPriceShip: 0,
                orderComfirmSms: "",
                orderBrandId: 1,
                orderFullName: "",
                orderPhone: "",
                orderEmail: "",
                orderDate: new Date(),
                orderNumber: "",
                totalPrice: 0,
                totalQuantity: 0,
                promoPrice: 0,
                distance: 0,
                distancePrice: 0,
                items: []
            };
        }
        $scope.SetDefaultShippingInfo = function () {
            $scope.shippingInfo = {
                orderFullName: $scope.order.orderFullName != "" ? $scope.order.orderFullName : "",
                orderAddress: $scope.order.orderAddress != "" ? $scope.order.orderAddress : "",
                orderPhone: $scope.order.orderPhone != "" ? $scope.order.orderPhone : "",
                orderDate: $scope.order.orderDate != "" ? $scope.order.orderDate : new Date()
            };
        }

        $scope.CalculateCard = function () {
            var total = 0;
            var sumQuantity = 0;
            $scope.order.items.forEach(item => {
                var price = item.unitPrice * item.quantity;
                total += price;
                sumQuantity += item.quantity;
            });
            $scope.order.totalPrice = total;
            $scope.order.totalQuantity = sumQuantity;
            $sessionStorage.order = $scope.order;
            console.log("order", $scope.order);
            console.log("totalPrice", $scope.order.totalPrice);
        }

        //add product to card
        $scope.AddProduct = function (product) {
            var itemExit = $scope.order.items.find(element => element.productId == product.productId);
            if (itemExit == undefined) {
                var item = {
                    quantity: 0,
                    unitPrice: 0,
                    productId: null,
                    productTitle: ""
                };
                item.productId = product.productId;
                item.productTitle = product.dishName;
                item.quantity = 1;
                item.unitPrice = product.unitPrice;
                $scope.order.items.push(item);
            }
            else {
                $scope.UpdateProductCard(itemExit);
            }
            $scope.CalculateCard();
            console.log("order card", $scope.order);
        }
        //UpdateProductCard
        $scope.UpdateProductCard = function (product) {
            this.order.items.forEach(item => {
                if (item.productId == product.productId) {
                    item.quantity += 1;
                }
            });
        }

        //AddItem
        $scope.AddItem = function (item, index) {
            this.order.items.forEach(element => {
                if (element.productId == item.productId) {
                    element.quantity += 1;
                }
            });
            $scope.CalculateCard();
        }
        //
        $scope.RemoveItem = function (item, index) {
            if (item.quantity <= 1) {
                $scope.order.items.splice(index, 1);
            }
            else {
                $scope.order.items.forEach(element => {
                    if (element.productId == item.productId) {
                        element.quantity -= 1;
                    }
                });
            }
            $scope.CalculateCard();
        }
        //CompleteProduct
        $scope.CompleteOrder = function () {
            if ($scope.session) {
                var url = baseApiUrl + "/api/orders";
                ApiServ.post({
                    url: url,
                    params: $scope.order,
                    success: function (result) {
                        if (result) {
                            $('#open-modal-success').trigger('click');
                            $scope.SetDefaultOrder();
                            $sessionStorage.order = $scope.order;
                            //$scope.CloseModalBooking();
                        }
                    },
                    failure: function (error) {
                        $('#open-modal-error').trigger('click');
                        //$scope.CloseModalBooking();
                    }
                });
                //$('#open-modal-success').trigger('click');
                $scope.CloseModalBooking();
            }
            else {
                $('#open-modal-no-login').trigger('click');
            }
            
        }
        $scope.CloseModalBooking = function () {
            $('#close-modal-booking').trigger('click');
        }

        $scope.CloseModalEditShippingInfo = function () {
            //$('#close-modal-edit-shipping-info').trigger('click');
            $('#modal-edit-shipping-info').modal('hide');
        }

        $scope.GoHome = function () {
            $window.location.href = '/home';
        }

        $scope.GoToLogin = function () {
            $window.location.href = '/login';
        }

        $scope.SaveShippingInfo = function () {
            $scope.order.orderFullName = $scope.shippingInfo.orderFullName;
            $scope.order.orderAddress = $scope.shippingInfo.orderAddress;
            $scope.order.orderPhone = $scope.shippingInfo.orderPhone;
            $scope.order.orderDate = $scope.shippingInfo.orderDate;
            $sessionStorage.order = $scope.order;
            $scope.GetDestinationMap($scope.order.orderAddress);
        }

        $scope.GetDestinationMap = function (address) {
            var data = {
                address: address
            }
            var url = baseApiUrl + "/GoogleData/GetCoordinate";
            ApiServ.post({
                url: url,
                params: data,
                success: function (result) {
                    console.log("GetDestinationMap", result);
                    if (result) {
                        $scope.destinationLat = result.latitude;
                        $scope.destinationLong = result.longitude;
                        $scope.SetViewMap($scope.destinationLat, $scope.destinationLong);
                        $scope.order.distance = $scope.DistanceMap($scope.originLat, $scope.originLong, $scope.destinationLat, $scope.destinationLong, "K");
                        var orderPriceShip = parseFloat($scope.order.distance) * 4000;
                        $scope.order.orderPriceShip = roundNumber(orderPriceShip);
                        console.log("$scope.order.orderPriceShip", roundNumber($scope.order.orderPriceShip));
                    }
                },
                failure: function (error) {
                    MessageServ.showError('Không tìm thấy địa điểm!');
                }
            });
        }
        function roundNumber(val) {
            var parsed = parseFloat(val, 10);
            if(parsed !== parsed) { return null; } // check for NaN
            var rounded = Math.round(parsed);
            return rounded;
        }

        $scope.DistanceMap = function(lat1, lon1, lat2, lon2, unit) {
            var radlat1 = Math.PI * lat1 / 180
            var radlat2 = Math.PI * lat2 / 180
            var theta = lon1 - lon2
            var radtheta = Math.PI * theta / 180
            var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
            if (dist > 1) {
                dist = 1;
            }
            dist = Math.acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;
            if (unit == "K") { dist = dist * 1.609344 };
            if (unit == "N") { dist = dist * 0.8684 };
            return dist
        }
        
        //Search Product
        $scope.SearchProduct = function () {
            $sessionStorage.searchValue = $scope.searchValue;
            $window.location.href = '/SearchProduct';
        }
        //Search Product
        $scope.GetProducts = function () {
            console.log("searchValue", $scope.searchValue);
            var url = "";
            if ($scope.searchValue == "") {
                url = baseApiUrl + "/api/productsearch/all";
            }
            else {
                url = baseApiUrl + "/api/productsearch/" + $scope.searchValue;
            }
            ApiServ.get({
                url: url,
                success: function (results) {
                    console.log("Search products", results);
                    if (results) {
                        $scope.listSearchProduct = results;
                        $scope.totalItem = results.length;
                    }
                },
                failure: function (error) {
                    MessageServ.showError('Search products không thành công !');
                }
            });
        }
        $scope.ClearSearch = function () {
            $scope.searchValue = "";
            $sessionStorage.searchValue = "";
            $("#searchValue").val("");
            $scope.GetProducts();
        }
        $scope.Logout = function () {
            $sessionStorage.session = null;
            $window.location.href = "/home";
        }
        this.OnInit();
    };
})(angular.module('myapp'));

//Modal Complete Order
(function (app) {
    'use strict';
    app.controller('ModalCompleteOrder', ModalCompleteOrder);
    ModalCompleteOrder.$inject = ['$rootScope', '$scope', '$uibModalInstance', '$sessionStorage', 'MessageServ', '$window', 'ApiServ'];
    function ModalCompleteOrder($rootScope, $scope, $uibModalInstance, $sessionStorage, MessageServ, $window, ApiServ) {
        this.load = function () {
        }
        this.load();
    };
})(angular.module('myapp'));