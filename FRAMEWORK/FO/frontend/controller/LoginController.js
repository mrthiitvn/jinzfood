﻿
(function (app) {
    'use strict';
    app.controller('LoginController', LoginController);
    LoginController.$inject = ['MessageServ', 'ApiServ', '$rootScope', '$scope', '$location', 'anchorSmoothScroll', '$filter', '$window', '$sessionStorage', '$uibModal', '$http', '$timeout'];
    function LoginController(MessageServ, ApiServ, $rootScope, $scope, $location, anchorSmoothScroll, $filter, $window, $sessionStorage, $uibModal, $http, $timeout) {
        var baseApiUrl = "http://beta.jinz.vn";
        $scope.baseImageUrl = 'http://beta.jinz.vn/images/';
        //$scope.session = {
        //    userName: ""
        //};
        $scope.userInfo = {};
        this.OnInit = function () {

        }
        $scope.LoginUser = function (users) {
            ApiServ.post({
                url: 'http://beta.jinz.vn/account/createtoken',
                params: users,
                success: function (result) {
                    if (result.token) {
                        $sessionStorage.userName = users.username;
                        //if ($sessionStorage.userName) {
                        //    $scope.session = {
                        //        userName: $sessionStorage.userName
                        //    };
                        //}
                        $scope.GetInfoUser($sessionStorage.userName);
                        
                    }
                },
                failure: function (error) {
                    MessageServ.showError('Mật khẩu hoặc tên đăng nhập không chính xác!');
                }
            });
        }

        $scope.GetInfoUser = function (email) {
            ApiServ.get({
                url: 'http://beta.jinz.vn/api/customer/' + email,
                success: function (results) {
                    $scope.session = results[0];
                    $sessionStorage.session = results[0];
                    console.log("$sessionStorage.session", $sessionStorage.session);
                    $window.location.href = '/home';
                },
                failure: function (error) {
                    MessageServ.showError('Mật khẩu hoặc tên đăng nhập không chính xác!');
                }
            });
        }
        this.OnInit();
    };
}) (angular.module('myapp'));
