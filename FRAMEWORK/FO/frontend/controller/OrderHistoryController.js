﻿(function (app) {
    'use strict';
    app.controller('OrderHistoryController', OrderHistoryController);
    OrderHistoryController.$inject = ['MessageServ', 'ApiServ', '$rootScope', '$scope', '$location', 'anchorSmoothScroll', '$filter', '$window', '$sessionStorage', '$uibModal'];
    function OrderHistoryController(MessageServ, ApiServ, $rootScope, $scope, $location, anchorSmoothScroll, $filter, $window, $sessionStorage, $uibModal) {
        var baseApiUrl = "http://beta.jinz.vn";
        $scope.baseImageUrl = 'http://beta.jinz.vn/images/';
        $scope.accountInfo = {};
        $scope.params = {};
        $scope.orderHistories = [];
        $scope.orderDetails = [];
        $scope.orderTotal = 0;
        $scope.session = null;
        this.OnInit = function () {
            if ($sessionStorage.session) {
                $scope.session = $sessionStorage.session;
            }
            $scope.SetParamInfo();
            $scope.GetOrderHistory();
            $scope.ViewDetailOrder(2);
        }

        $scope.SetParamInfo = function () {
            $scope.params = {
                //userId: $scope.session ? $scope.session.id : 0,
                userId: 16,
                statusId: 0,
                orderDateFrom: "2018-04-10",
                orderDateTo: "2018-07-28"
            }
        }

        $scope.GetOrderHistory = function () {
            var url = baseApiUrl + "/api/orders/GetOrderHistory";
            ApiServ.post({
                url: url,
                params: $scope.params,
                success: function (results) {
                    console.log("Lấy danh sách lịch sử order thành công", results);
                    if (results.length > 0) {
                        $scope.orderHistories = results;
                    }
                },
                failure: function (error) {
                    MessageServ.showError('Lấy danh sách lịch sử order không thành công !');
                }
            });
        }

        $scope.ViewDetailOrder = function (id) {
            var params = { brandId: 0, groupId: 0, orderId: 2 }
            var url = baseApiUrl + "/api/orders/GetOrderItemsBySearch";
            ApiServ.post({
                url: url,
                params: params,
                success: function (results) {
                    console.log("Chi tiết orders", results);
                    if (results.length > 0) {
                        $scope.orderDetails = results;
                        var total = 0;
                        $scope.orderDetails.forEach(element => {
                            total += element.unitPrice;
                        });
                        $scope.orderTotal = total;
                        $('#open-modal-order-history').trigger('click');
                        var modalinstance = $uibmodal.open({
                            animation: true,
                            arialabelledby: 'modal-title',
                            ariadescribedby: 'modal-body',
                            templateurl: 'detailsorder.html',
                            controller: 'modaldetailsorder',
                            resolve: {
                                orderdetails: function () {
                                    return $scope.orderdetails;
                                }
                            }
                        });
                        modalinstance.result.then(function (selecteditem) {
                            //neu nguoi dung bam nut chon
                        }, function () {
                            //neu nguoi dung bam cancel
                        });
                    }
                },
                failure: function (error) {
                    MessageServ.showError('Chi tiết orders không thành công !');
                }
            });
        }
        this.OnInit();
    };
})(angular.module('myapp'));

(function (app) {
    'use strict';
    app.controller('ModalDetailsOrder', ModalDetailsOrder);
    ModalDetailsOrder.$inject = ['$rootScope', '$scope', 'orderDetails', '$uibModalInstance', 'ApiServ', '$sessionStorage'];
    function ModalDetailsOrder($rootScope, $scope, orderDetails, $uibModalInstance, ApiServ, $sessionStorage) {
        this.load = function () {
            console.log("orderDetails test", orderDetails);
        }

        $scope.ok = function () {
            $uibModalInstance.close();
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        this.load();
    };
})(angular.module('myapp'));
