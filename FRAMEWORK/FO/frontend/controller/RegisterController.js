﻿
(function (app) {
    'use strict';
    app.controller('RegisterController', RegisterController);
    RegisterController.$inject = ['MessageServ', 'ApiServ', '$rootScope', '$scope', '$sce', '$window'];
    function RegisterController(MessageServ, ApiServ, $rootScope, $scope, $sce, $window) {
        $rootScope.trustHtml = function (html) {
            return $sce.trustAsHtml(html);
        }
        //Create Contact
        $scope.CreateAccount = function (account) {
            //console.log(account);
            ApiServ.post({
                url: 'http://beta.jinz.vn/account/createcustomer',
                params: account,
                success: function (result) {
                    if (result == 'Success') {
                        MessageServ.showSuccess('Đăng ký thành công !');
                        $window.location.href = '/login';
                    }

                },
                failure: function (error) {
                    MessageServ.showError('Gửi thông tin bị lỗi !');
                }
            });
        }
    };
})(angular.module('myapp'));
