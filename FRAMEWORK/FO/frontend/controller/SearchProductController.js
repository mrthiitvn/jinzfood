﻿(function (app) {
    'use strict';
    app.controller('SearchProductController', SearchProductController);
    SearchProductController.$inject = ['MessageServ', 'ApiServ', '$scope', '$location', 'anchorSmoothScroll', '$filter', '$window', '$sessionStorage'];
    function SearchProductController(MessageServ, ApiServ, $scope, $location, anchorSmoothScroll, $filter, $window, $sessionStorage, $uibModal, $http) {
        var baseApiUrl = "http://beta.jinz.vn";
        $scope.baseImageUrl = 'http://beta.jinz.vn/images/';
        $scope.searchValue = "";
        $scope.listSearchProduct = [];
        $scope.totalItem = 0;
        this.OnInit = function () {
            if ($sessionStorage.searchValue) {
                $scope.searchValue = $sessionStorage.searchValue;
            }
            $scope.GetProducts();
            $scope.paging = { maxSize: 9, itemsPerPage: 12, current: 1 };//paging
        }
        //Search Product
        $scope.SearchProduct = function () {
            $sessionStorage.searchValue = $scope.searchValue;
            $window.location.href = '/SearchProduct';
        }
        //Search Product
        $scope.GetProducts = function () {
            //console.log("searchValue", $scope.searchValue);
            var url = "";
            if ($scope.searchValue == "") {
                url = baseApiUrl + "/api/productsearch/all";
            }
            else {
                url = baseApiUrl + "/api/productsearch/" + $scope.searchValue;
            }
            ApiServ.get({
                url: url,
                success: function (results) {
                    //console.log("Search products", results);
                    if (results) {
                        $scope.listSearchProduct = results;
                        $scope.totalItem = results.length;
                    }
                },
                failure: function (error) {
                    MessageServ.showError('Search products không thành công !');
                }
            });
        }
        $scope.ClearSearch = function () {
            $scope.searchValue = "";
            $sessionStorage.searchValue = "";
            $("#searchValue").val("");
            $scope.GetProducts();
        }
        //Init Card
        $scope.InitCard = function () {
            if ($sessionStorage.order) {
                $scope.order = $sessionStorage.order;
            }
            else {
                $scope.SetDefaultOrder();
            }
            $scope.CalculateCard();
        }

        $scope.SetDefaultOrder = function () {
            $scope.order = {
                userId: 0,
                orderId: 0,
                orderAddress: "",
                orderDistance: 0,
                orderPriceShip: 0,
                orderComfirmSms: "",
                orderBrandId: 0,
                orderFullName: "",
                orderPhone: "",
                orderEmail: "",
                orderDate: new Date(),
                orderNumber: "",
                totalPrice: 0,
                totalQuantity: 0,
                promoPrice: 0,
                items: []
            };
        }
        $scope.SetDefaultShippingInfo = function () {
            $scope.shippingInfo = {
                orderFullName: $scope.order.orderFullName != "" ? $scope.order.orderFullName : "",
                orderAddress: $scope.order.orderAddress != "" ? $scope.order.orderAddress : "",
                orderPhone: $scope.order.orderPhone != "" ? $scope.order.orderPhone : "",
                orderDate: $scope.order.orderDate != "" ? $scope.order.orderDate : new Date()
            };
        }

        $scope.CalculateCard = function () {
            var total = 0;
            var sumQuantity = 0;
            $scope.order.items.forEach(item => {
                var price = item.unitPrice * item.quantity;
                total += price;
                sumQuantity += item.quantity;
            });
            $scope.order.totalPrice = total;
            $scope.order.totalQuantity = sumQuantity;
            $sessionStorage.order = $scope.order;
            //console.log("order", $scope.order);
            //console.log("totalPrice", $scope.order.totalPrice);
        }

        //add product to card
        $scope.AddProduct = function (product) {
            var itemExit = $scope.order.items.find(element => element.productId == product.productId);
            if (itemExit == undefined) {
                var item = {
                    quantity: 0,
                    unitPrice: 0,
                    productId: null,
                    productTitle: ""
                };
                item.productId = product.productId;
                item.productTitle = product.dishName;
                item.quantity = 1;
                item.unitPrice = product.unitPrice;
                $scope.order.items.push(item);
            }
            else {
                $scope.UpdateProductCard(itemExit);
            }
            $scope.CalculateCard();
            //console.log("order card", $scope.order);
        }
        //UpdateProductCard
        $scope.UpdateProductCard = function (product) {
            this.order.items.forEach(item => {
                if (item.productId == product.productId) {
                    item.quantity += 1;
                }
            });
        }

        //AddItem
        $scope.AddItem = function (item, index) {
            this.order.items.forEach(element => {
                if (element.productId == item.productId) {
                    element.quantity += 1;
                }
            });
            $scope.CalculateCard();
        }
        //
        $scope.RemoveItem = function (item, index) {
            if (item.quantity <= 1) {
                $scope.order.items.splice(index, 1);
            }
            else {
                $scope.order.items.forEach(element => {
                    if (element.productId == item.productId) {
                        element.quantity -= 1;
                    }
                });
            }
            $scope.CalculateCard();
        }
        //CompleteProduct
        $scope.CompleteOrder = function () {
            var url = baseApiUrl + "/api/orders";
            ApiServ.post({
                url: url,
                params: $scope.order,
                success: function (result) {
                    if (result) {
                        $('#open-modal-success').trigger('click');
                        $scope.SetDefaultOrder();
                        $sessionStorage.order = $scope.order;
                        //$scope.CloseModalBooking();
                    }
                },
                failure: function (error) {
                    $('#open-modal-error').trigger('click');
                    //$scope.CloseModalBooking();
                }
            });
            //$('#open-modal-success').trigger('click');
            $scope.CloseModalBooking();
        }
        $scope.CloseModalBooking = function () {
            $('#close-modal-booking').trigger('click');
        }

        $scope.CloseModalEditShippingInfo = function () {
            //$('#close-modal-edit-shipping-info').trigger('click');
            $('#modal-edit-shipping-info').modal('hide');
        }

        $scope.GoHome = function () {
            $window.location.href = '/home';
        }

        $scope.SaveShippingInfo = function () {
            $scope.order.orderFullName = $scope.shippingInfo.orderFullName;
            $scope.order.orderAddress = $scope.shippingInfo.orderAddress;
            $scope.order.orderPhone = $scope.shippingInfo.orderPhone;
            $scope.order.orderDate = $scope.shippingInfo.orderDate;
            $sessionStorage.order = $scope.order;
        }
        this.OnInit();
    };
})(angular.module('myapp'));