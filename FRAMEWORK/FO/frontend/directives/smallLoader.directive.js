﻿(function (app) {
    'use strict';
    app.directive('smallLoader', smallLoader);
    function smallLoader() {
        return {
            template: '<div style="text-align:center;"><img src="/Content/images/library/loading.gif" width="20%" /></div>'
        }
    }
})(angular.module('myapp'));