﻿(function (app) {
    'use strict';
    app.filter("jsonDate", function () {
        var re = /\/Date\(([0-9]*)\)\//;
        return function (x) {
            if (x == null) return null;
            var m = x.match(re);
            if (m) return new Date(parseInt(m[1]));
            else return null;
        };
    });
    //SO DIEM DUNG
    //app.filter('filterLightBySDD', filterLightBySDD);
    //function filterLightBySDD() {
    //    return function (FlightChuyenDiModel, SoDiemDung) {
    //        if (FlightChuyenDiModel != null && FlightChuyenDiModel.length > 0) {
    //            var filtered = [];
    //            if ((SoDiemDung != null && SoDiemDung >= 0)) {
    //                for (var i = 0; i < FlightChuyenDiModel.length; i++) {
    //                    var item = FlightChuyenDiModel[i];
    //                    if ((SoDiemDung == 0 || SoDiemDung == 1) && item.SoDiemDung == SoDiemDung)
    //                        filtered.push(item);
    //                    if (SoDiemDung > 1 && item.SoDiemDung > SoDiemDung)
    //                        filtered.push(item);
    //                }
    //            }
    //            return filtered;
    //        } else return FlightChuyenDiModel;
    //    };
    //}
    //GIO KHOI HANH
    app.filter('filterLightByGKH', filterLightByGKH);
    function filterLightByGKH() {
        return function (FlightChuyenDiModel, GioKhoiHanh) {
            if (FlightChuyenDiModel != null && FlightChuyenDiModel.length > 0) {
                var filtered = [];
                if ((GioKhoiHanh != null && GioKhoiHanh != '')) {
                    var khoihanh = GioKhoiHanh.substr(0, 5);//00:00
                    var dennoi = GioKhoiHanh.substr(6, 5);//06:00
                    dennoi = dennoi != '00:00' ? dennoi : '23:59';
                    for (var i = 0; i < FlightChuyenDiModel.length; i++) {
                        var item = FlightChuyenDiModel[i];
                        if (khoihanh <= item.DepartureTime && item.DepartureTime <= dennoi) {
                            filtered.push(item);
                            //console.log('STT: ' + i + '---' + khoihanh + ' <= ' + item.DepartureTime + ' --- ' + item.DestinationTime + ' <= ' + dennoi);
                        }
                    }
                    return filtered;
                }
            }
            return FlightChuyenDiModel;
        };
    }
    //HANG HANG KHONG
    app.filter('filterLightByHHK', filterLightByHHK);
    function filterLightByHHK() {
        return function (FlightChuyenDiModel, HangHangKhong) {
            if (FlightChuyenDiModel != null && FlightChuyenDiModel.length > 0) {
                var filtered = [];
                if (HangHangKhong == 'Vietjet Air') HangHangKhong = 'Vietjet';
                else if (HangHangKhong == 'Vietnam Airlines') HangHangKhong = 'VietnamAirline';
                if ((HangHangKhong != null && HangHangKhong != '')) {
                    for (var i = 0; i < FlightChuyenDiModel.length; i++) {
                        var item = FlightChuyenDiModel[i];
                        if (item.Airline == HangHangKhong) filtered.push(item);
                    }
                    return filtered;
                }
            }
            return FlightChuyenDiModel;
        };
    }
    //KHOANG GIA
    app.filter('filterLightByKG', filterLightByKG);
    function filterLightByKG() {
        return function (FlightChuyenDiModel, KhoangGia) {
            if (FlightChuyenDiModel != null && FlightChuyenDiModel.length > 0) {
                var filtered = [];
                if ((KhoangGia != null && KhoangGia != '')) {
                    for (var i = 0; i < FlightChuyenDiModel.length; i++) {
                        var item = FlightChuyenDiModel[i];
                        var totalPrice = item.PriceAdult + item.PriceChild + item.PriceInfant;
                        if ((KhoangGia.minValue <= totalPrice) && (totalPrice <= KhoangGia.maxValue))
                            filtered.push(item);
                    }
                    return filtered;
                }
            }
            return FlightChuyenDiModel;
        };
    }

})(angular.module('myapp'));