(function(app) {
    'use strict';
    app.factory('ApiServ', ApiServ);
    ApiServ.$inject = ['$http', 'MessageServ', '$rootScope', '$location', '$window'];

    function ApiServ($http, MessageServ, $rootScope, $location, $window) {
        var self = this;
        var services = {
            get: get,
            post: post,
            del: del,
            upload: upload,
            put: put
        };
        
        function get(data) {
            if($rootScope.tokenData.state===0){
                self.initializeToken();
                return setTimeout(function(){get(data);},1000);
            }
            if($rootScope.tokenData.state===1){
                return setTimeout(function(){get(data);},1000);
            }
            return $http.get(data.url, {
                params: data.params
                })
                .then(function(result) {
                    if (data.success !== null && typeof(data.success) === 'function') {
                        data.success(result.data);
                    }
                }, function(error) {
                    //if (error.status === '401') {
                    //    MessageServ.showError('Không thể thiết lập kết nối đến máy chủ API ('+error.status+')');
                    //    $rootScope.tokenData.state=0;
                    //} else if (error.status === '501') {
                    //    MessageServ.showError('Lỗi máy chủ, vui lòng liên hệ quản trị', 10000);
                    //} else if (error.status === '404') {
                    //    MessageServ.showError('Không tìm thấy tài liệu được yêu cầu', 10000);
                    //} else if (error.status === '-1') {
                    //    MessageServ.showError('Lỗi máy chủ, vui lòng liên hệ quản trị', 10000);
                    //} else if (data.failure !== null && typeof(data.failure) === 'function') {
                    //    data.failure(error);
                    //}
                });
        }

        function del(data) {
            if($rootScope.tokenData.state===0){
                self.initializeToken();
                return setTimeout(function(){del(data);},1000);
            }
            if($rootScope.tokenData.state===1){
                return setTimeout(function(){del(data);},1000);
            }
            return $http.delete(data.url, {
                    params: data.params
                })
                .then(function(result) {
                    if (data.success !== null && typeof(data.success) === 'function') {
                        data.success(result.data);
                    }
                }, function(error) {
                    if (error.status === '401') {
                        MessageServ.showError('Không thể thiết lập kết nối đến máy chủ API ('+error.status+')');
                        $rootScope.tokenData.state=0;
                    } else if (error.status === '501') {
                        MessageServ.showError('Lỗi máy chủ, vui lòng liên hệ quản trị', 10000);
                    } else if (error.status === '404') {
                        MessageServ.showError('Không tìm thấy tài liệu được yêu cầu', 10000);
                    } else if (error.status === '-1') {
                        MessageServ.showError('Lỗi máy chủ, vui lòng liên hệ quản trị', 10000);
                    } else if (data.failure !== null && typeof(data.failure) === 'function') {
                        data.failure(error);
                    }
                });
        }

        function post(data) {
            if($rootScope.tokenData.state===0){
                self.initializeToken();
                return setTimeout(function(){post(data);},1000);
            }
            if($rootScope.tokenData.state===1){
                return setTimeout(function(){post(data);},1000);
            }
            return $http.post(data.url, data.params)
                .then(function(result) {
                    if (data.success !== null && typeof(data.success) === 'function') {
                        data.success(result.data);
                    }
                }, function(error) {
                    if (error.status === '401') {
                        MessageServ.showError('Không thể thiết lập kết nối đến máy chủ API ('+error.status+')');
                        $rootScope.tokenData.state=0;
                    } else if (error.status === '501') {
                        MessageServ.showError('Lỗi máy chủ, vui lòng liên hệ quản trị', 10000);
                    } else if (error.status === '404') {
                        MessageServ.showError('Không tìm thấy tài liệu được yêu cầu', 10000);
                    } else if (error.status === '-1') {
                        MessageServ.showError('Lỗi máy chủ, vui lòng liên hệ quản trị', 10000);
                    } else if (data.failure !== null && typeof(data.failure) === 'function') {
                        data.failure(error);
                    }
                });
        }

        function put(data) {
            if($rootScope.tokenData.state===0){
                self.initializeToken();
                return setTimeout(function(){post(data);},1000);
            }
            if($rootScope.tokenData.state===1){
                return setTimeout(function(){post(data);},1000);
            }
            return $http.put(data.url, data.params)
                .then(function(result) {
                    if (data.success !== null && typeof(data.success) === 'function') {
                        data.success(result.data);
                    }
                }, function(error) {
                    if (error.status === '401') {
                        MessageServ.showError('Không thể thiết lập kết nối đến máy chủ API ('+error.status+')');
                        $rootScope.tokenData.state=0;
                    } else if (error.status === '501') {
                        MessageServ.showError('Lỗi máy chủ, vui lòng liên hệ quản trị', 10000);
                    } else if (error.status === '404') {
                        MessageServ.showError('Không tìm thấy tài liệu được yêu cầu', 10000);
                    } else if (error.status === '-1') {
                        MessageServ.showError('Lỗi máy chủ, vui lòng liên hệ quản trị', 10000);
                    } else if (data.failure !== null && typeof(data.failure) === 'function') {
                        data.failure(error);
                    }
                });
        }
        
        function upload(data) {
            if($rootScope.tokenData.state===0){
                self.initializeToken();
                return setTimeout(function(){upload(data);},1000);
            }
            if($rootScope.tokenData.state===1){
                return setTimeout(function(){upload(data);},1000);
            }
            if (data.files !== null && data.files.length > 0) {
                var fd = new FormData();
                for (var i = 0; i < data.files.length; i++) {
                    fd.append("file", data.files[i]);
                }
                return $http.post(data.url, fd, {
                    withCredentials: true,
                    headers: {
                        'Content-Type': undefined
                    },
                    transformRequest: angular.identity
                }).then(function(result) {
                    if (data.success !== null) {
                        data.success(result.data);
                    }
                }, function(error) {
                    if (error.status === '401') {
                        MessageServ.showError('Không thể thiết lập kết nối đến máy chủ API ('+error.status+')');
                        $rootScope.tokenData.state=0;
                    } else if (error.status === '501') {
                        MessageServ.showError('Lỗi máy chủ, vui lòng liên hệ quản trị', 10000);
                    } else if (error.status === '404') {
                        MessageServ.showError('Không tìm thấy tài liệu được yêu cầu', 10000);
                    } else if (error.status === '-1') {
                        MessageServ.showError('Lỗi máy chủ, vui lòng liên hệ quản trị', 10000);
                    } else if (data.failure !== null && typeof(data.failure) === 'function') {
                        data.failure(error);
                    }
                });
            }
        }
        return services;
    }
})(angular.module('myapp'));