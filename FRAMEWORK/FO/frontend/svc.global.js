(function (app) {
    'use strict';
    app.factory('GlobalServ', GlobalServ);
    GlobalServ.$inject = ['$location', '$rootScope', '$window'];
    function GlobalServ($location, $rootScope, $window) {
        var services = {
            setTitle: setTitle,
            dateFormat: 'dd-MMM-yyyy',
            refresh: refresh,
            redirect: redirect
        };
        function setTitle(title) {
            if (title != null && title != '') {
                document.title = $rootScope.appTitle + ' - ' + title;
            }
        }

        function redirect(url, params) {
            if (params == null) {
                $location.path(url);
            }
            else {
                $location.path(url).search(params);
            }
        }

        function refresh(url) {
            if (url == null) {
                $window.location.href = $location.path();
            }
            else {
                $window.location.href = url;
            }
        }

        return services;
    }
})(angular.module('myapp'));