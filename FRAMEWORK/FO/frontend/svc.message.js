(function(app) {
    'use strict';
    app.factory('MessageServ', MessageServ);
    function MessageServ() {
        Noty.overrideDefaults({
            type: 'info',
            layout: 'topRight',
            theme: 'metroui',
            timeout: 5000,
            progressBar: true,
            closeWith: ['click', 'button'],
            animation: {
                open  : 'animated bounceInRight',
                close : 'animated bounceOutRight',
                easing: 'fade',
                speed : 500
            },
            id: false,
            force: false,
            killer: false,
            queue: 'global',
            container: false,
            buttons: [],
            sounds: {
              sources: [],
              volume: 1,
              conditions: []
            },
            titleCount: {
              conditions: []
            },
            modal: false
            }
        );
        var services = {
            showSuccess: showSuccess,
            showError: showError,
            showWarning: showWarning,
            showInfo: showInfo,
            showConfirm: showConfirm,
            showAlert: showAlert
        };

        function showSuccess(message, timeOut) {
            new Noty({
                text: message,
                type:'success',
                timeout: timeOut,
                template: '<div class="alert alert-success"><i class="fa fa-check"></i><span class="noty_text"></span><div class="noty_close"></div></div>'
            }).show();
        }

        function showError(error, timeOut) {
            if (Array.isArray(error)) {
                error.forEach(function(err) {
                    new Noty({
                        text: err,
                        type:'error',
                        timeout: timeOut,
                        template: '<div class="alert alert-danger"><i class="fa fa-close"></i><span class="noty_text"></span><div class="noty_close"></div></div>'
                    }).show();
                });
            } else {
                new Noty({
                    text: error,
                    type:'error',
                    timeout: timeOut,
                    template: '<div class="alert alert-danger"><i class="fa fa-close"></i><span class="noty_text"></span><div class="noty_close"></div></div>'
                }).show();
            }
        }

        function showWarning(message, timeOut) {
            new Noty({
                text: message,
                type:'warning',
                timeout: timeOut,
                template: '<div class="alert alert-warning"><i class="fa fa-exclamation-triangle"></i><span class="noty_text"></span><div class="noty_close"></div></div>'
            }).show();
        }

        function showInfo(message, timeOut) {
            var n = new Noty({
                text: message,
                type:'info',
                timeout: timeOut
            });
            n.show();
        }

        function showConfirm(message, confirmCallBack, cancelCallBack) {
            bootbox.confirm(message, function(result) {
                if(result){
                    if(confirmCallBack!==undefined) confirmCallBack();
                }else{
                    if(cancelCallBack!==undefined) cancelCallBack();
                }
            }); 
        }

        function showAlert(message, confirmCallBack) {
            bootbox.alert(message, function() {
                if(confirmCallBack!==undefined) confirmCallBack();
            }); 
        }
        return services;
    }
})(angular.module('myapp'));