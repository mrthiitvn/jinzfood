//animate css plugin
$.fn.extend({
    animateCss: function (animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        this.addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
        });
        return this;
    }
});
//main event-handlers
$(function(){
    $('.flexslider').flexslider({
        animation: "slide",
        directionNav: false
    });
    //datepicker
    $('#ngayDi').datepicker({
        numberOfMonths: [1,2],
        dateFormat: 'dd/mm/yy',
        minDate: 0
    });
    $('#ngayVe').datepicker({
        numberOfMonths: [1,2],
        dateFormat: 'dd/mm/yy',
        minDate: 0
    });
    $('#ngaySinh').datepicker({
        numberOfMonths: 1,
        dateFormat: 'dd/mm/yy',
        maxDate: 0
    });
    $('#ngayDi').change(function(){
        $('#ngayVe').val(null);
        $('#ngayVe').datepicker('destroy').datepicker({
            numberOfMonths: [1,2],
            dateFormat: 'dd/mm/yy',
            minDate: new Date($('#ngayDi').datepicker('getDate'))
        });
    });
    $('.ngaySinh').datepicker({
        numberOfMonths: 1,
        dateFormat: 'dd/mm/yy',
        maxDate: 0
    });
    //switch loại vé 1 chiều, khứ hồi
    var toggleNgayVe = function(){
        if($('#loaiVe1').is(':checked')){
            $('#ngayVe').prop('disabled',true);
        }else{
            $('#ngayVe').prop('disabled',false);
        }
    };
    $('input[name=loaiVe]').change(function(){
        toggleNgayVe();
    });
    toggleNgayVe();
    //danh sach doi tac
    $('#logo-carousel').slick({
        variableWidth: false,
        dots: true,
        infinite: true,
        speed: 2000,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    dots: true
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
    //custom select dropdown list
    //$('select').selectric();
    //switch location
    //$('#bookingFormSwitchLocation').click(function(){
    //   var temp = $('#diemKhoiHanh').val();
    //   $('#diemKhoiHanh').val($('#diemDen').val()).change().selectric();
    //   $('#diemDen').val(temp).change().selectric();
    //   console.log($('#diemKhoiHanh').val()+ "-" + $('#diemDen').val());
    //});
    //kiểm tra số lượng đặt ghế
    $('#soNguoiLon').change(function(){
        var temp = $(this).val();
        $(this).val(Math.min(temp,9-$('#soTreEm').val()));
    }).change();
    $('#soTreEm').change(function(){
        var temp = $(this).val();
        $(this).val(Math.min(temp,9-$('#soNguoiLon').val()));
    }).change();
    $('#soTreSoSinh').change(function(){
        var temp = $(this).val();
        $(this).val(Math.min(temp,$('#soNguoiLon').val()));
    }).change();
    //scrollMagic
    /*var HomeController = new ScrollMagic.Controller({globalSceneOptions: {}});
    new ScrollMagic.Scene({
        triggerElement: "#servicesHeader",
        offset: -100,
        duration: window.innerHeight
    })
    .setClassToggle(".service", "active") // add class toggle
    .addTo(HomeController);
    new ScrollMagic.Scene({
        triggerElement: "#subcribe",
        offset: 0,
        duration: window.innerHeight
    })
    .setClassToggle("#subcribeLeft", "active") // add class toggle
    .addTo(HomeController);
    new ScrollMagic.Scene({
        triggerElement: "#subcribe",
        offset: 0,
        duration: window.innerHeight
    })
    .setClassToggle("#subcribeRight", "active") // add class toggle
    .addTo(HomeController);*/
    
    //sticky header
    $(document).scroll(function(){
        if($(document).scrollTop()>40){
            $('#headPlaceHolder').height($('#head').height()+20);
            $('#head').addClass('sticky');
            if(!$('#head').hasClass('started')){
                //$('#head').animateCss('bounceInDown');
                $('#head').addClass('started');
            }
            $('#headPlaceHolder').addClass('show');
        }else{
            $('#head').removeClass('sticky').removeClass('started');
            $('#headPlaceHolder').removeClass('show');
        }
    });
    //totop
    $('.totop').click(function(){
        $('body')
                .stop()
                .animate({
                    scrollTop: 0
                },'1000');
        return false;
    });
    //tự động qua input mới ở form tìm vé
    $('#diemKhoiHanh').blur(function(){
        //$('#diemDen').focus();
    });
    $('#diemDen').blur(function(){
        //$('#ngayDi').focus();
    });
    $('#ngayDi').change(function () {
        if ($('#loaiVe2').is(':checked')) {
            //$('#ngayVe').focus();
        }
    });
    //tooltips
    $('[data-toggle="tooltip"]').tooltip(); 
    //toogle form
    $('#form-toggle').click(function () {
        if ($('#homeBookingFormInner').hasClass('hidden')) {
            $('#homeBookingFormInner').removeClass('hidden');
        } else {
            $('#homeBookingFormInner').addClass('hidden');
        }
    });
});
