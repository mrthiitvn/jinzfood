﻿using System;
using System.Collections.Generic;

namespace FRAMEWORK.Models.AUTH
{
    // Models returned by AccountController actions.

    public class ExternalLoginViewModel
    {
        public string Name { get; set; }

        public string Url { get; set; }

        public string State { get; set; }
    }

    public class ManageInfoViewModel
    {
        public string LocalLoginProvider { get; set; }

        public string Email { get; set; }

        public IEnumerable<UserLoginInfoViewModel> Logins { get; set; }

        public IEnumerable<ExternalLoginViewModel> ExternalLoginProviders { get; set; }
    }

    public class UserInfoViewModel
    {
        public string Email { get; set; }

        public bool HasRegistered { get; set; }

        public string LoginProvider { get; set; }
    }

    public class UserLoginInfoViewModel
    {
        public string LoginProvider { get; set; }

        public string ProviderKey { get; set; }
    }

    public class UserDataViewModel
    {
        public string UserName { get; set; }
        public string PasswordHash { get; set; }
        public List<string> Roles { get; set; }
        public int Id { get; set; }
        public int Status { get; set; }
        public int NV_ID { get; set; }
        public string NV_HOTEN { get; set; }
        public string NV_EMAIL { get; set; }
        public string NV_DIENTHOAI { get; set; }
        public string NV_AVATAR { get; set; }
        public string NV_DIACHI { get; set; }
        public bool? NV_GIOITINH { get; set; }
        public bool NV_DOIMATKHAU { get; set; }
    }

    public class LoginViewModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    public class SignUpViewModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
    }
    
    public class ChangePasswordViewModel
    {
        public int Id { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
    }
}
