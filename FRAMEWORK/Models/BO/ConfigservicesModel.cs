﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FRAMEWORK.Models.BO
{
    public class ConfigservicesModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<int> Status { get; set; }
    }
    public class ChangeServiceStatusModel
    {
        public int ID { get; set; }
        public Nullable<int> Status { get; set; }
    }
}
