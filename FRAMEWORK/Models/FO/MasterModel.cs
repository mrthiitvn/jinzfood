﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FRAMEWORK.Models.FO
{
    public class BannersModel
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Image { get; set; }
        public string Link { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<int> SortOrder { get; set; }
    }
    public class SignUpEmailModel
    {
        public string ConfigData { get; set; }
    }
    public class AirPortModel
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public Nullable<int> CityId { get; set; }
        public bool IsDomestic { get; set; }
        public bool IsInternational { get; set; }
        public System.DateTime Updated { get; set; }
        public string UpdateUser { get; set; }
        public string CityName { get; set; }
        public string Region { get; set; }
    }
    public class ChuyenBayModel
    {
        public ChuyenBayVietNamModel VietNam { get; set; }
        public List<ChuyenBayQuocTeModel> QuocTe { get; set; }
    }
    public class ChuyenBayVietNamModel
    {
        public List<VungMienModel> MienNam { get; set; }
        public List<VungMienModel> MienTrung { get; set; }
        public List<VungMienModel> MienBac { get; set; }
    }
    public class ChuyenBayQuocTeModel
    {
        public string Ma { get; set; }
        public string Ten { get; set; }
    }
    public class VungMienModel
    {
        public string Ma { get; set; }
        public string Ten { get; set; }
    }
    public class HanhLyModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public Nullable<int> Value { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<int> Status { get; set; }
    }
    public class PaymentMethodModel
    {
        public string PaymentMethod { get; set; }
        //public string PaxInfo { get; set; }
        public int OrderID { get; set; }
    }
    public class PartnerCodeModel
    {
        public string PartnerCode { get; set; }
        public int OrderID { get; set; }
    }
    public class SaveOrderModel
    {
        public int OrderID { get; set; }
        public int Result { get; set; }
    }
    public class KhachHangModel
    {
        public string CustomerTitle { get; set; }
        public string CustomerFirstName { get; set; }
        public string CustomerLastName { get; set; }
        public string CustomerMobile { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerEmail { get; set; }
        public string FlightInfo { get; set; }
        public string CompanyInfoVAT { get; set; }
        public string PaxInfo { get; set; }
        public string CustomerFullName
        {
            get
            {
                return CustomerLastName + CustomerFirstName;
            }
        }
        public ThongTinChuyenBayModel DaChonChuyenBay { get; set; }
    }
    public class ThongTinChuyenBayModel
    {
        public ChuyenBay ChuyenBayDi { get; set; }
        public ChuyenBay ChuyenBayVe { get; set; }
        public string MaDatCho { get; set; }
        public long TongThanhToan { get; set; }
        public long TongTien { get; set; }
        public long VAT { get; set; }
    }
    public class ChuyenBay
    {
        public string Airline { get; set; }
        public string AirlineCode { get; set; }
        public string FlightCode { get; set; }

        public string OriginAirport { get; set; }
        public string OriginAirportCode { get; set; }
        public DateTime DepartureDateTime { get; set; }

        public string DestinationAirport { get; set; }
        public string DestinationAirportCode { get; set; }
        public DateTime DestinationDateTime { get; set; }
    }
    public class NguoiLon
    {
        public int? QuyDanh { get; set; }
        public string Ho { get; set; }
        public string Ten { get; set; }
        public DateTime? NgaySinh { get; set; }
        public string CMND { get; set; }
        public int? HanhLyBayDi { get; set; }
        public int? HanhLyBayVe { get; set; }
    }
    public class TreEm
    {
        public int? QuyDanh { get; set; }
        public string Ho { get; set; }
        public string Ten { get; set; }
        public DateTime? NgaySinh { get; set; }
        public int? HanhLyBayDi { get; set; }
        public int? HanhLyBayVe { get; set; }
    }
    public class EmBe
    {
        public int? QuyDanh { get; set; }
        public string Ho { get; set; }
        public string Ten { get; set; }
        public DateTime? NgaySinh { get; set; }
    }
    public class ThongTinHanhKhach
    {
        public List<NguoiLon> NguoiLon { get; set; }
        public List<TreEm> TreEm { get; set; }
        public List<EmBe> EmBe { get; set; }
    }
    public class XemLaiDonHang
    {
        public string FlightInfo { get; set; }
        public string PaxInfo { get; set; }
        public string PaymentMethod { get; set; }
        public string CompanyInfoVAT { get; set; }
        public string StatusName { get; set; }//trang thai don hang
    }
    public class AddFee_Discount_Model
    {
        public int ID { get; set; }
        public string FlightCode { get; set; }
        public decimal AddFee { get; set; }
        public decimal Discount { get; set; }
        public string Passenger { get; set; }
        public bool IsDomestic { get; set; }
    }
}
