﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FRAMEWORK.Models.Fo
{
    public class SignUpEmailModel
    {
        public int ConfigID { get; set; }
        public string ConfigCode { get; set; }
        public string ConfigDescription { get; set; }
        public string ConfigGroup { get; set; }
        public string ConfigData { get; set; }
        public Nullable<int> ConfigValue { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
    }
}
