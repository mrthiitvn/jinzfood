﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using log4net.Config;

[assembly: OwinStartup(typeof(FRAMEWORK.Startup))]
[assembly: XmlConfigurator(ConfigFile = "Web.config", Watch = true)]
namespace FRAMEWORK
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
